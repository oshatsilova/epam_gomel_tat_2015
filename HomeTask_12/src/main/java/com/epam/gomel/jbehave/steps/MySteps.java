package com.epam.gomel.jbehave.steps;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import com.epam.gomel.tat.service.MailGuiService;
import com.epam.gomel.tat.ui.Browser;
import org.apache.commons.lang3.RandomStringUtils;
import org.jbehave.core.annotations.*;

public class MySteps {

    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    LoginGuiService loginGuiService = new LoginGuiService();
    MailGuiService mailGuiService = new MailGuiService();
    Letter letter = LetterBuilder.getDefaultLetter();
    String userLogin, userPassword;

    @BeforeScenario
    public void prepareBrowser() {
        Browser.get();
    }

    @Given("a correct user login")
    public void getUserDefaultLogin() {
        userLogin = defaultAccount.getLogin();
    }

    @Given("a correct user password")
    public void getUserDefaultPassword() {
        userPassword = defaultAccount.getPassword();
    }

    @Given("a wrong user password")
    public void getUserWrongLogin() {
        userPassword = defaultAccount.getPassword() + RandomStringUtils.randomAlphanumeric(3);
    }

    @Given("access to account")
    public void loginIntoMailBox() {
        getUserDefaultLogin();
        getUserDefaultPassword();
        doLogIntoMailAccount();
    }

    @When("User do log")
    public void doLogIntoMailAccount() {
        loginGuiService.openLoginPage().login(userLogin, userPassword);
    }

    @When("User send mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Then("User have access to Mailbox account")
    public void checkAccessToAccount() {
        assert mailGuiService.isAccountAccess();
        mailGuiService.checkAccountLink(defaultAccount.getEmail());
    }

    @Then("User haven't access to Mailbox account")
    public void checkNoAccessToAccount() {
        assert !mailGuiService.isAccountAccess();
    }

    @Then("User see error message")
    public void checkLoginErrorMessage() {
        loginGuiService.checkErrorLoginMessage();
    }

    @Then("mail is in $Page list")
    public void checkMailPresentOnPage(@Named("Page") String pageName) {
        mailGuiService.checkMailPresentOnPage(letter, pageName);
    }

    @AfterScenario
    public void closeBrowser() {
        Browser.get().close();
    }
}
