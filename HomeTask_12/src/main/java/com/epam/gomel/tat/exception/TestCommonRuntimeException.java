package com.epam.gomel.tat.exception;

public class TestCommonRuntimeException extends RuntimeException {

    public TestCommonRuntimeException(String s) {
        super(s);
    }

}
