package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.ui.Browser;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class FailLoginPage extends MailboxBasePage {

    private static final String errorMessage = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public Boolean isErrorMessagePresent() {
        Logger.stepInfo("Verify is login error message present");
        return Browser.get().isTextPresent(errorMessage);
    }
}
