package com.epam.gomel.tat.exception;

public class TestMailContentMatchException extends RuntimeException {

    public TestMailContentMatchException(String errorMessage) {
        super(errorMessage);
    }

}
