package com.epam.gomel.tat.ui;

import com.epam.gomel.tat.reporting.Logger;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class Browser {

    public static final String DOWNLOAD_DIR = "R:\\Epam\\Rep\\epam_gomel_tat_2015\\lesson_8\\Home Task 8\\DOWNLOAD\\";//FileUtils.getTempDirectoryPath();
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT = 200;
    private static final int COMMAND_DEFAULT_TIMEOUT = 5;
    private static final int WAIT_ELEMENT_TIMEOUT = 20;
    private static final int AJAX_TIMEOUT = 100;
    public static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private WebDriver driver;
    private boolean augmented;
    private static Map<Thread, Browser> instances = new HashMap<>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        WebDriver driver = null;
        driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        //driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Thread currentThread = Thread.currentThread();
        currentThread.setName("№" + Integer.toString(instances.size() + 1));
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String URL) {
        Logger.stepInfo("Open page: " + URL);
        driver.get(URL);
        instances.get(Thread.currentThread()).takeScreenshot();

    }

    public void close() {
        Logger.stepInfo("Close browser");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();

            } catch (Exception e) {
                Logger.error("Cannot close browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void click(By locator) {
        elementHighlight(driver.findElement(locator)).click();
    }

    public void type(By locator, String text) {
        WebElement mailTextInput = driver.findElement(locator);
        mailTextInput.clear();
        elementHighlight(mailTextInput).sendKeys(text);
    }

    public void attachFile(By locator, String path) {

        waitForElementPresent(locator);
        driver.findElement(locator).sendKeys(path);
        instances.get(Thread.currentThread()).takeScreenshot();
    }

    public boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void waitForElementPresent(final By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isElementPresent(locator);
            }
        });
    }

    public boolean isTextPresent(String text) {
        Logger.stepInfo("Verify is text \"" + text + "\" present");
        instances.get(Thread.currentThread()).takeScreenshot();
        try {
            driver.getPageSource().contains(text);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public String getElementText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void takeScreenshot() {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.stepInfo("Screenshot saved:  " + "  -  " + String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")).replace("Screenshot", copy.getName()));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot");
        }
    }

    public WebElement elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
        return element;
    }

}
