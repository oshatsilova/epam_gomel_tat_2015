package com.epam.gomel.tat.bo.common;

import org.apache.commons.lang3.RandomStringUtils;

public class AccountBuilder {
    public static Account getDefaultAccount() {
        Account account = new Account(DefaultAccounts.DEFAULT_USER.getAccount());
        return account;
    }

    public static Account getAccountWithIncorrectPassword() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphanumeric(5));
        return account;
    }
}
