package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.Element;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class ComposeMailPage extends MailboxBasePage {
    private static final Element mail_to_input_field = new Element(By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']"));
    private static final Element mail_subject_field = new Element(By.name("subj"));
    private static final Element mail_text_field = new Element(By.id("compose-send"));
    private static final Element mail_attach_field = new Element(By.xpath("//input[@name='att']"));
    private static final Element submit_send_mail_button = new Element(By.id("compose-submit"));

    public MailboxBasePage sendMail(Letter letter) {
        Logger.stepInfo("Send mail with subject: " + letter.getSubject());
        mail_to_input_field.waitForPresent().type(letter.getReceiver());
        mail_subject_field.waitForPresent().type(letter.getSubject());
        mail_text_field.waitForPresent().type(letter.getContent());
        if (letter.getAttach() != null) {
            mail_attach_field.attachFile(letter.getAttach());
        }
        submit_send_mail_button.click();
        Browser.get().waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}
