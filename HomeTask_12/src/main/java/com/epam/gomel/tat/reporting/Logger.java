package com.epam.gomel.tat.reporting;

public class Logger {

    private static org.apache.log4j.Logger My_log = org.apache.log4j.Logger.getLogger(Logger.class);
    private static String tab = "     ";


    public static void error(String s) {
        My_log.error(s);
        
    }

    public synchronized static void error(String s, Throwable e) {
        My_log.error(s, e);
    }

    public synchronized static void fatal(String s) {
        My_log.fatal(s);
    }

    public synchronized static void fatal(String s, Throwable t) {
        My_log.fatal(s, t);
    }

    public synchronized static void warn(String s) {
        My_log.warn(s);
    }

    public synchronized static void warn(String s, Throwable t) {
        My_log.warn(s, t);
    }

    public synchronized static void info(String s) {
        My_log.info(s);
    }

    public synchronized static void stepInfo(String s) {
        My_log.info(tab + s);
    }

    public synchronized static void info(String s, Throwable t) {
        My_log.info(s, t);
    }

    public synchronized static void debug(String s) {
        My_log.debug(s);
    }

    public synchronized static void debug(String s, Throwable t) {
        My_log.debug(s, t);
    }

    public synchronized static void trace(String s) {
        My_log.trace(s);
    }

    public synchronized static void trace(String s, Throwable t) {
        My_log.trace(s, t);
    }
}
