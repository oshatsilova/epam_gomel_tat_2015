package com.epam.gomel.tat.service;

import com.epam.gomel.tat.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.pages.FailLoginPage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.ui.Browser;

public class LoginGuiService {

    private static final String BASE_URL = "http://mail.yandex.ru";

    public MailLoginPage openLoginPage() {
        Browser.get().open(BASE_URL);
        return new MailLoginPage();
    }

    public void checkErrorLoginMessage() {
        if (!new FailLoginPage().isErrorMessagePresent()) {
            throw new TestCommonRuntimeException("Login with incorrect password is failed");
        }
    }
}