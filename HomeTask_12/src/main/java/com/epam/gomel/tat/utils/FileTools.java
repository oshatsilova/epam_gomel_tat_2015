package com.epam.gomel.tat.utils;

import com.epam.gomel.tat.reporting.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by oshatsilova on 26.03.2015.
 */
public class FileTools {

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = FileTools.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
        }
        return null;
    }

}
