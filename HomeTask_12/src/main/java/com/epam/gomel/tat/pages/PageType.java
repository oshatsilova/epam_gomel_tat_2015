package com.epam.gomel.tat.pages;

import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 16/04/2015.
 */
public enum PageType {
    INBOX(By.xpath("//a[@class='b-folders__folder__link'][@href='#inbox']")),
    SENT(By.xpath("//a[@class='b-folders__folder__link'][@href='#sent']"));


    private By pageLinkLocator;

    PageType(By alias) {
        this.pageLinkLocator = alias;
    }

    public static By getTypeByAlias(String alias) {
        for (PageType type : PageType.values()) {
            if (type.name().equals(alias)) {
                return type.getPageLinkLocator();
            }
        }
        throw new RuntimeException("No such page name value");
    }

    public By getPageLinkLocator() {
        return pageLinkLocator;
    }
}
