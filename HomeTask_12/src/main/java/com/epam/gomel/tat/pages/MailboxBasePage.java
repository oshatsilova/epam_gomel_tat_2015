package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.Element;
import com.epam.gomel.tat.reporting.Logger;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailboxBasePage {

    private Element mailSubjectLocator = new Element("div.block-messages:not([style=\"display: none;\"]) span.b-messages__subject[title=\"%s\"]");
    private static final Element MAIL_ACCOUNT_LINK = new Element(By.xpath("//span[@class='header-user-name js-header-user-name']"));
    private static final Element compose_mail_button = new Element(By.xpath("//a[@href='#compose']"));

    public String getMailAccountLinkText() {
        return MAIL_ACCOUNT_LINK.getText();
    }

    public boolean isMailAccountLinkPresent() {
        return MAIL_ACCOUNT_LINK.isPresent();
    }


    public void openPage(String pageName) {
        Element pageLink = new Element(PageType.getTypeByAlias(pageName));
        Logger.stepInfo("Open page" + pageName);
        pageLink.waitForPresent().click();
    }

    public ComposeMailPage openComposeMailPage() {
        Logger.stepInfo("Open ComposeMailPage");
        compose_mail_button.waitForPresent().click();
        return new ComposeMailPage();
    }

    public boolean isMailPresent(String subject) {
        return mailSubjectLocator.formatLocator(subject).isPresent();
    }

}
