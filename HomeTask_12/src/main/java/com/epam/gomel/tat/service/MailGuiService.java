package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.reporting.Logger;

public class MailGuiService {

    public boolean isAccountAccess() {
        return new MailboxBasePage().isMailAccountLinkPresent();
    }

    public void checkAccountLink(String userMail) {
        MailboxBasePage mailboxBasePage = new MailboxBasePage();
        if (userMail.equals(mailboxBasePage.getMailAccountLinkText())) {
            Logger.stepInfo("Success login to Mail Account");
        } else {
            Logger.stepInfo("Wrong user account link text");
        }

    }

    public void sendMail(Letter letter) {
        new MailboxBasePage().openComposeMailPage().sendMail(letter);
    }

    public void checkMailPresentOnPage(Letter letter, String pageName) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openPage(pageName);
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is on " + pageName + " page list");
        if (!mailbox.isMailPresent(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is not on " + pageName + " page list");
        }
    }

}
