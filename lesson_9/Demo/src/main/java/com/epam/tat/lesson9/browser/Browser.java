package com.epam.tat.lesson9.browser;

import atu.testng.reports.ATUReports;
import com.epam.tat.lesson9.GlobalConfig;
import com.epam.tat.lesson9.utils.FileTools;
import com.epam.tat.lesson9.utils.Logger;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Browser {

//    private static final Logger logger = Logger.getLogger(Browser.class);

    private static final String DOWNLOAD_DIR = "D:\\TEMP";
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt, image/jpeg";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    public static final int WAIT_ELEMENT_TIMEOUT = 20;
    public static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    public static final File screenshotDirectory = new File(SCREENSHOT_DIRECTORY);

    private WebDriver driver;

    private boolean augmented;

    private static Map<Thread, Browser> instances = new HashMap<>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = GlobalConfig.instance().getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                try {
                    driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }

                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", FileTools.getCanonicalPathToResourceFile("/chromedriver.exe"));
                driver = new ChromeDriver();
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case HTMLUNIT:
                DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
                driver = new HtmlUnitDriver(capabilities);
                ((HtmlUnitDriver) driver).setJavascriptEnabled(true);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
        }
       // ATUReports.setWebDriver(driver);
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String url) {
        Logger.info("Open: " + url);
        driver.get(url);
        takeScreenshot();
    }

    public static void kill() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }


    /**
     * Set implicit timeout in seconds
     *
     * @param timeout
     */
    public void configImplicitWait(int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public void click(By locator) {
        Logger.info("Click: '"+ locator.toString()+"'");
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.click();
        takeScreenshot();
    }

    public void type(By locator, String text) {
        Logger.info("Type: " + text + " into '"+ locator.toString()+"'");
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
        takeScreenshot();
    }

    public void attachFile(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

    public void dragAndDrop(By locator, int xOffset, int yOffset) {
        WebElement draggable = driver.findElement(locator);
        new Actions(driver).dragAndDropBy(draggable, xOffset, yOffset).build().perform();
    }

    public void mouseOver(By locator) {
        WebElement element = driver.findElement(locator);
        new Actions(driver).moveToElement(element).build().perform();
    }

    public void doubleClick(By locator) {
        WebElement element = driver.findElement(locator);
        new Actions(driver).doubleClick(element).build().perform();
    }

    public void waitForPresent(final By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void takeScreenshot() {
        if (GlobalConfig.instance().getBrowserType() == BrowserType.REMOTE &&
                !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public String acceptAlert() {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        alert.accept();
        return text;
    }

    public String clickAndHandleAlert(By locator) {
        click(locator);
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        alert.accept();
        return text;
    }

    public String clickAndHandleConfirm(By locator, boolean confirm) {
        click(locator);
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        if (confirm) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text + " (confirmed: " + confirm + ")";
    }

    public String clickAndHandlePrompt(By locator, String answer) {
        click(locator);
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        alert.sendKeys(answer);
        alert.accept();
        return text + " (answered: " + answer + ")";
    }

    public void waitForVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void switchToWindow(String handler) {
        driver.switchTo().window(handler);
    }

    public void switchToFrame(By locator) {
        WebElement element = driver.findElement(locator);
        driver.switchTo().frame(element);
    }

    public void switchToFrame(String frame) {
        driver.switchTo().frame(frame);
    }

    public String openNewWindowOnClick(By locator) {
        String current = driver.getWindowHandle();
        click(locator);
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
        }
        return current;
    }

    public String[] getAllWindowNames() {
        String current = driver.getWindowHandle();
        List<String> attributes = new ArrayList<String>();
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
            attributes.add(((JavascriptExecutor) driver).executeScript("return document.getEl").toString());
        }
        driver.switchTo().window(current);
        return attributes.toArray(new String[attributes.size()]);
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("alert = function(msg) {var tmp=msg; return true}");
            }
        });
    }

    public void submit(By locator) {
        driver.findElement(locator).submit();
    }


}
