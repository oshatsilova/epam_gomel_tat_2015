package com.epam.gomel.tat.pages.YandexDiskPages;


import com.epam.gomel.tat.Element;

import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 06.04.2015.
 */
public class DiskLoginPage {
    public static final String PASPORT_URL = "https://passport.yandex.ru";
    public static final String DISK_URL = "https://disk.yandex.ru/client/disk";
    public Element loginFieldLocator = new Element(By.name("login"));
    public Element passwordFieldLocator = new Element(By.name("passwd"));
    public Element submitLoginButtonLocator = new Element(By.xpath("//button[@type='submit']"));

    public DiskLoginPage open() {
        Browser.get().open(PASPORT_URL);
        return this;
    }

    public DiskBasePage login(String login, String password) {
        loginFieldLocator.waitForPresent().type(login);
        passwordFieldLocator.waitForPresent().type(password);
        submitLoginButtonLocator.click();
        Browser.get().open(DISK_URL);
        return new DiskBasePage();
    }

}
