package com.epam.gomel.tat.bo.page;

/**
 * Created by oshatsilova on 05.04.2015.
 */
public class PageBuilder {
    public static Page getInboxPage() {
        Page page = new Page(MailPages.INBOX_PAGE.getPage());
        return page;
    }

    public static Page getSentPage() {
        Page page = new Page(MailPages.SENT_PAGE.getPage());
        return page;
    }
    public static Page getTrashPage() {
        Page page = new Page(MailPages.TRASH_PAGE.getPage());
        return page;
    }
    public static Page getSpamPage() {
        Page page = new Page(MailPages.SPAM_PAGE.getPage());
        return page;
    }
}
