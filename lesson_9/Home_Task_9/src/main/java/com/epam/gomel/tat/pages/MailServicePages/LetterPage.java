package com.epam.gomel.tat.pages.MailServicePages;

import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.exception.TestMailContentMatchException;
import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.utils.FileTools;
import org.openqa.selenium.By;

import java.io.IOException;

/**
 * Created by oshatsilova on 22.03.2015.
 */
public class LetterPage extends MailboxBasePage {
    private static final By MAIL_RECIPIENT_LOCATOR = By.cssSelector("div.block-message-head:not([style=\"display: none;\"]) span.b-message-head__email");
    private static final By MAIL_SUBJECT_LOCATOR = By.xpath("//span[@class='js-message-subject js-invalid-drag-target']");
    private static final By MAIL_TEXT_INPUT_LOCATOR = By.xpath("//div[@class='b-message-body__content']/p");
    private static String DOWNLOAD_ATTACHMENT = "//span[@class='js-message-subject js-invalid-drag-target'][.='%s']/ancestor::div[@class='b-layout__inner b-layout__inner_type_message js-message-inner']//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']";

    public void downloadAttachment(String subject) throws IOException {
        browser.click(By.xpath(String.format(DOWNLOAD_ATTACHMENT, subject)));
    }

    public void checkContent(Letter letter) {
        Logger.stepInfo("checkContent: " + MAIL_RECIPIENT_LOCATOR.toString());
        browser.waitForElementPresent(MAIL_RECIPIENT_LOCATOR);
        String receiver = browser.getElementText(MAIL_RECIPIENT_LOCATOR);
        String currentReceiver = receiver.substring(2, receiver.length() - 1);
        if (!currentReceiver.equals(letter.getReceiver())) {
            throw new TestMailContentMatchException("Receiver does not match to expected one: " + letter.getReceiver());
        }
        if (!browser.getElementText(MAIL_SUBJECT_LOCATOR).equals(letter.getSubject())) {
            throw new TestMailContentMatchException("Subject does not match to expected one: " + letter.getSubject());
        }
        if (!browser.getElementText(MAIL_TEXT_INPUT_LOCATOR).equals(letter.getContent())) {
            throw new TestMailContentMatchException("Content does not match to expected one: " + letter.getContent());
        }
        if (letter.getAttach() != null) {
            try {
                downloadAttachment(letter.getSubject());
                boolean ff = FileTools.compareFiles(letter.getAttach());
                if (!FileTools.compareFiles(letter.getAttach())) {
                    throw new TestMailContentMatchException("Attachment does not match to expected one: " + letter.getAttach());
                }
            } catch (Exception e) {
            }
        }
    }


}