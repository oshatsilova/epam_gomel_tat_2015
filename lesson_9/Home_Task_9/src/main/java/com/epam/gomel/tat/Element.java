package com.epam.gomel.tat;

import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 07.04.2015.
 */
public class Element {
    private By elementLocator;
    private String strElementLocator;

    public Element(By locator) {
        this.elementLocator = locator;
    }

    public Element(String strLocator) {
        this.strElementLocator = strLocator;
    }

    public Element formatLocator(String subject) {
        this.elementLocator = By.cssSelector(String.format(strElementLocator, subject));
        return this;
    }

    public By getLocator() {
        return this.elementLocator;
    }

    public void click() {
        Logger.stepInfo("Click on: " + elementLocator);
        Browser.get().click(elementLocator);
        Browser.get().takeScreenshot();
        Browser.get().waitForAjaxProcessed();
    }

    public Element waitForPresent() {
        Logger.stepInfo("Wait for element present: " + elementLocator);
        Browser.get().waitForElementPresent(elementLocator);
        Browser.get().waitForAjaxProcessed();
        return this;
    }

    public void type(String text) {
        Logger.stepInfo("Type text: " + text + " to element " + elementLocator);
        Browser.get().type(elementLocator, text);
        Browser.get().takeScreenshot();
    }
    public void attachFile(String path) {
        Logger.stepInfo("Send path for attach file: " + path + " to element " + elementLocator);
        Browser.get().attachFile(elementLocator, path);
        Browser.get().takeScreenshot();
    }

    public boolean isPresent() {
        Browser.get().waitForAjaxProcessed();
        Logger.stepInfo("Is element present: " + elementLocator);
        Browser.get().takeScreenshot();
        return Browser.get().isElementPresent(elementLocator);
    }
    public void doubleClick(){
        Logger.stepInfo("Dobleclick on element "+elementLocator);
        Browser.get().doubleClick(elementLocator);
        Browser.get().waitForAjaxProcessed();
    }
    public String getText(){
        Logger.stepInfo("Get text from: " + elementLocator);
        Browser.get().takeScreenshot();
        return Browser.get().getElementText(elementLocator);
    }
    public Element waitFoVisible(){
        Logger.stepInfo("Wait for element is visible : " + elementLocator);
        Browser.get().waitForVisible(elementLocator);
        return this;
    }
    public void dragAndDropTo(By target){
        Logger.stepInfo("Drag and drop element "+elementLocator+" to element "+target);
        Browser.get().waitForElementPresent(target);
        Browser.get().takeScreenshot();
        Browser.get().dragAndDrop(elementLocator, target);
        Browser.get().waitForAjaxProcessed();
    }
    public Element mouseOver(){
        Logger.stepInfo("Mouse over the element "+elementLocator);
        Browser.get().mouseOver(elementLocator);
        Browser.get().takeScreenshot();
        return this;
    }
}
