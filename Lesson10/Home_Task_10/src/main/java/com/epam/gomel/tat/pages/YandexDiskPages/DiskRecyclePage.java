package com.epam.gomel.tat.pages.YandexDiskPages;

import com.epam.gomel.tat.Element;
import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 07.04.2015.
 */
public class DiskRecyclePage {
    private Element fileOnRecyclePage = new Element("div[class=\"ns-view-listing b-listing b-listing_view_icons b-listing_group-by_none ns-view-visible\"] div[class~=\"b-resource_view_icons\"][title=\"%s\"]");
    //private Element fileOnRecyclePage = new Element("div.ns-view-listing.b-listing.b-listing_view_icons.b-listing_group-by_none.ns-view-visible div[data-nb=\"resource\"][title=\"%s\"]");
    private Element recoverButton = new Element("div.nb-panel__actions.b-aside__actions button.nb-button._nb-small-button._init.ns-action.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only[data-click-action=\"resource.restore\"][data-params*=\"%s\"]");
    private String recycleTirle = "Корзина";
    public void recoverFileFromRecycle(String name) {
        fileOnRecyclePage.formatLocator(name)
                .waitForPresent()
                .mouseOver()
                .click();
        recoverButton.formatLocator(name).waitForPresent().click();
    }

    public boolean isFileInRecycle(String name) {
        Browser.get().waitForTextPresent(recycleTirle);
        return fileOnRecyclePage.formatLocator(name).isPresent();
    }
}
