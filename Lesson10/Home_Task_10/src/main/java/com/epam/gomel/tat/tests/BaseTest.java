package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Created by oshatsilova on 4/01/2015.
 */
public class BaseTest {

    @BeforeClass
    public void prepareBrowser() {
        Browser.get();
    }

    @AfterClass
    public void stopBrowser() {
        Browser.get().close();
    }
}
