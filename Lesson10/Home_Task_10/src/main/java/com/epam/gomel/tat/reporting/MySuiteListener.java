package com.epam.gomel.tat.reporting;

import com.epam.gomel.tat.runner.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Created by oshatsilova on 4/1/2015.
 */
public class MySuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.instance().getParallelMode().getAlias());
        suite.getXmlSuite().setThreadCount(GlobalConfig.instance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
