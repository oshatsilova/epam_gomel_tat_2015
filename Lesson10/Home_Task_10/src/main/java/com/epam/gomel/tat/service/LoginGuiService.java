package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.pages.MailServicePages.FailLoginPage;
import com.epam.gomel.tat.pages.MailServicePages.MailLoginPage;
import com.epam.gomel.tat.pages.MailServicePages.MailboxBasePage;
import com.epam.gomel.tat.reporting.Logger;

public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.stepInfo("Login to account: " + account);
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getMailAccountLinkName();
        if (userEmail == null || !userEmail.equals(account.getEmail())) {
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public void checkLoginWithIncorrectPassword(Account account) {
        Logger.stepInfo("Login to account: " + account + " with wrong password");
        new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        if (!new FailLoginPage().isErrorMessagePresent()) {
            throw new TestCommonRuntimeException("Login with incorrect password is not");
        }
    }
}