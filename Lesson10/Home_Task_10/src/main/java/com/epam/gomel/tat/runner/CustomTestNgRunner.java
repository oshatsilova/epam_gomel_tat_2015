package com.epam.gomel.tat.runner;

import com.epam.gomel.tat.reporting.CustomTestNgListener;
import com.epam.gomel.tat.reporting.MySuiteListener;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oshatsilova on 3/30/2015.
 */
public class CustomTestNgRunner {

    private CustomTestNgRunner(String[] args) {
        parseCli(args);
    }

    public static void main(String[] args) {
        new CustomTestNgRunner(args).runTests();
    }

    private void parseCli(String[] args) {
        CmdLineParser parser = new CmdLineParser(GlobalConfig.instance());
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.err.println();
            System.exit(1);
        }
    }

    private void runTests() {
        TestNG tng = new TestNG();

        CustomTestNgListener listner = new CustomTestNgListener();
        tng.addListener(listner);

        MySuiteListener suiteListener = new MySuiteListener();
        tng.addListener(suiteListener);

        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");

        List<String> files = new ArrayList<>();
        files.addAll(GlobalConfig.instance().getSuites());
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);


        tng.run();

    }

}
