package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.page.Page;
import com.epam.gomel.tat.bo.page.PageBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.pages.MailServicePages.MailboxBasePage;
import com.epam.gomel.tat.pages.MailServicePages.ActionsWithMail;
import com.epam.gomel.tat.reporting.Logger;

public class MailGuiService {

    public void sendMail(Letter letter) {
        new MailboxBasePage().openComposeMailPage().sendMail(letter);
    }

    public void checkMailPresentOnPage(Letter letter, Page page) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openPage(page);
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is on " + page.getPageName());
        if (!mailbox.isMailPresent(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is not on " + page.getPageName() + " list");
        }
    }


    public void checkMailNotPresentOnPage(Letter letter, Page page) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openPage(page);
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is not on " + page.getPageName());
        if (mailbox.isMailPresent(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is on " + page.getPageName() + " list");
        }
    }

    public void deleteMailI(Letter letter) {
        ActionsWithMail mailbox = new ActionsWithMail();
        mailbox.deleteMail(letter.getSubject());
    }

    public void sendMailToSpam(Letter letter) {
        ActionsWithMail mailbox = new ActionsWithMail();
        mailbox.openPage(PageBuilder.getInboxPage());
        mailbox.sendMailToSpam(letter.getSubject());
    }

    public void unSpamMail(Letter letter) {
        ActionsWithMail mailbox = new ActionsWithMail();
        mailbox.openPage(PageBuilder.getSpamPage());
        mailbox.unSpamMail(letter.getSubject());
    }

    public void checkMailContent(Letter letter) {
        MailboxBasePage maibox = new MailboxBasePage();
        maibox.openPage(PageBuilder.getInboxPage());
        maibox.openLetter(letter.getSubject()).checkContent(letter);
    }


}
