package com.epam.gomel.tat.pages.MailServicePages;

import com.epam.gomel.tat.pages.MailServicePages.MailboxBasePage;
import com.epam.gomel.tat.reporting.Logger;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class FailLoginPage extends MailboxBasePage {

    private static final String errorMessage = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public Boolean isErrorMessagePresent() {
        Logger.stepInfo("Verify is login error message present");
        return browser.isTextPresent(errorMessage);
    }
}
