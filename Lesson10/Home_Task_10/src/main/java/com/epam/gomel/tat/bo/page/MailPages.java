package com.epam.gomel.tat.bo.page;

import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 05.04.2015.
 */
public enum MailPages {
    INBOX_PAGE("INBOX_PAGE", By.xpath("//a[@class='b-folders__folder__link'][@href='#inbox']")),
    SENT_PAGE("SENT_PAGE", By.xpath("//a[@class='b-folders__folder__link'][@href='#sent']")),
    TRASH_PAGE("TRASH_PAGE", By.xpath("//a[@class='b-folders__folder__link'][@href='#trash']")),
    SPAM_PAGE("SPAM_PAGE", By.xpath("//a[@class='b-folders__folder__link'][@href='#spam']"));

    private Page currentPage;

    MailPages(String pageName, By pageLinkLocator) {
        currentPage = new Page(pageName, pageLinkLocator);
    }

    public Page getPage() {
        return currentPage;
    }

}
