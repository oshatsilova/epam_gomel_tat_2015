package com.epam.gomel.tat.pages.MailServicePages;

import com.epam.gomel.tat.Element;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailLoginPage extends MailboxBasePage {

    private static final String BASE_URL = "http://mail.yandex.ru";
    private Element loginField = new Element(By.name("login"));
    private Element passwordField = new Element(By.name("passwd"));
    private Element submitButtonLocator = new Element(By.xpath("//button[@type='submit']"));

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        loginField.waitForPresent().type(login);
        passwordField.waitForPresent().type(password);
        submitButtonLocator.click();
        return new MailboxBasePage();
    }

}
