package com.epam.gomel.tat.pages.MailServicePages;

import com.epam.gomel.tat.Element;
import com.epam.gomel.tat.bo.page.Page;
import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.ui.Browser;
import org.openqa.selenium.By;


/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailboxBasePage {

    private Element mailSubjectLocator = new Element("div.block-messages:not([style=\"display: none;\"]) span.b-messages__subject[title=\"%s\"]");
    public static final By MAIL_ACCOUNT_LINK_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final String CHECKBOX_LOCATOR = "//div[@class='block-messages-list-box b-layout__first-pane']/div[@class='block-messages'][not(@style='display: none;')]//span[@class='b-messages__subject'][.='%s']/ancestor::*[@data-action='mail.message.show-or-select']//input[@class='b-messages__message__checkbox__input']";

    protected Browser browser;

    public MailboxBasePage() {
        this.browser = Browser.get();
    }

    public String getMailAccountLinkName() {
        browser.waitForElementPresent(MAIL_ACCOUNT_LINK_LOCATOR);
        return browser.getElementText(MAIL_ACCOUNT_LINK_LOCATOR);
    }

    public void openPage(Page page) {
        Logger.stepInfo("Open page" + page.getPageName());
        browser.waitForVisible(page.getPageLinkLocator());
        browser.click(page.getPageLinkLocator());
        browser.waitForAjaxProcessed();
    }

    public void selectMail(String subject) {
        Logger.stepInfo("Select mail with subject: " + subject);
        browser.waitForVisible(By.xpath(String.format(CHECKBOX_LOCATOR, subject)));
        browser.click(By.xpath(String.format(CHECKBOX_LOCATOR, subject)));
    }

    public ComposeMailPage openComposeMailPage() {
        Logger.stepInfo("Open ComposeMailPage");
        browser.waitForElementPresent(COMPOSE_BUTTON_LOCATOR);
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public LetterPage openLetter(String subject) {
        Logger.stepInfo("Open Letter with " + subject + " subject");
        mailSubjectLocator.formatLocator(subject).waitForPresent().click();
        return new LetterPage();
    }

    public boolean isMailPresent(String subject) {
        return mailSubjectLocator.formatLocator(subject).isPresent();
    }

}
