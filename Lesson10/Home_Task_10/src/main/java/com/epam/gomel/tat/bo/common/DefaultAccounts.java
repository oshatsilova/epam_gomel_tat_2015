package com.epam.gomel.tat.bo.common;

public enum DefaultAccounts {
    DEFAULT_USER("shatsilova", "00223344", "shatsilova@yandex.ru");

    private Account account;

    DefaultAccounts(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getAccount() {
        return account;
    }
}
