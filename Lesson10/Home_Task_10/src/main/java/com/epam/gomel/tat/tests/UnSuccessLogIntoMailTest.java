package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class UnSuccessLogIntoMailTest extends BaseTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getAccountWithIncorrectPassword();

    @Test(description = "UnSuccess mail login with wrong password")
    public void successLogin() {
        loginGuiService.checkLoginWithIncorrectPassword(defaultAccount);
    }

}
