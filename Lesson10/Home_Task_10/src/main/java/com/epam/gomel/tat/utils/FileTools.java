package com.epam.gomel.tat.utils;

import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.ui.Browser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by oshatsilova on 26.03.2015.
 */
public class FileTools {
    private static final int FILE_NAME_LENGTH = 15;
    private static final int FILE_DOWNLOAD_TIMEOUT = 20;
    private static final String PATH_TO_ATTACHED_FILE = "RES\\";

    public static File createFile() throws IOException {
        String fileName = RandomStringUtils.randomAlphanumeric(FILE_NAME_LENGTH) + ".txt";
        String fileContent = UUID.randomUUID().toString();
        File attachFile = new File(PATH_TO_ATTACHED_FILE + fileName);
        FileUtils.writeStringToFile(attachFile, fileContent);
        return attachFile;
    }
    public static String createRandomFile() throws IOException {
        String fileName = RandomStringUtils.randomAlphanumeric(FILE_NAME_LENGTH) + ".txt";
        String fileContent = UUID.randomUUID().toString();
        File attachFile = new File(PATH_TO_ATTACHED_FILE + fileName);
        FileUtils.writeStringToFile(attachFile, fileContent);
        return attachFile.getAbsolutePath();
    }


    public static boolean compareFiles(String fulPathToAttachFile) throws IOException {
        File attachedFile = new File(fulPathToAttachFile);
        File downloadedFile = new File(Browser.DOWNLOAD_DIR + attachedFile.getName());
        waitForDownload(downloadedFile);
        Logger.stepInfo("Compare files");
        return FileUtils.contentEquals(attachedFile, downloadedFile);
    }
    public static void waitForDownload(File file) {
        Logger.stepInfo("Wait for file downloaded");
        new FluentWait<File>(file).withTimeout(FILE_DOWNLOAD_TIMEOUT, TimeUnit.SECONDS).until(new com.google.common.base.Predicate<File>() {
            @Override
            public boolean apply(File file) {
                return file.exists();
            }
        });
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = FileTools.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
        }
        return null;
    }

}
