package com.epam.gomel.tat.bo.mail;

import com.epam.gomel.tat.bo.common.DefaultAccounts;
import com.epam.gomel.tat.utils.FileTools;
import java.io.IOException;
import org.apache.commons.lang3.RandomStringUtils;


/**
 * Created by oshatsilova on 23.03.2015.
 */
public class LetterBuilder {

    public static Letter getDefaultLetter() {
        Letter letter = new Letter();
        letter.setReceiver(DefaultAccounts.DEFAULT_USER.getAccount().getEmail());
        letter.setSubject(RandomStringUtils.randomAlphanumeric(20));
        letter.setContent(RandomStringUtils.randomAlphanumeric(100));
        return letter;
    }

    public static Letter getLetterWithAttach() throws IOException {
        Letter letter = getDefaultLetter();
        letter.setAttach(FileTools.createRandomFile());
        return letter;
    }
}
