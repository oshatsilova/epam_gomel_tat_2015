package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailLoginPage extends AbstractBasePage {

    public static final String BASE_URL = "http://mail.yandex.ru";

    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    private static final By ENTER_BUTTON_LOCATOR = By.xpath("//input[@type='submit']");

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage loginWithCorrectCredentials() {
        browser.type(LOGIN_INPUT_LOCATOR, currentUser.userLogin);
        browser.type(PASSWORD_INPUT_LOCATOR, currentUser.userPassword);
        browser.click(ENTER_BUTTON_LOCATOR);
        return new MailboxBasePage();
    }

    public FailLoginPage loginWithWrongPassword() {
        browser.type(LOGIN_INPUT_LOCATOR, currentUser.userLogin);
        browser.type(PASSWORD_INPUT_LOCATOR, Double.toString(Math.random() * 100000000));
        browser.click(ENTER_BUTTON_LOCATOR);
        return new FailLoginPage();
    }

}
