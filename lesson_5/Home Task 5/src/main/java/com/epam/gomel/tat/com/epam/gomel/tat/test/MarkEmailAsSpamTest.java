package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 15.03.2015.
 */
public class MarkEmailAsSpamTest extends MailTestsBase {
    @Test(description = "Mark email as a spam")
    public void markEmailAsSpam() {
        MailboxBasePage mailbox = new MailLoginPage()
                .open()
                .loginWithCorrectCredentials()
                .openInboxPage()
                .openComposeMailPage()
                .sendRandomMail(false); // is mail with attachment or not
        mailbox.openInboxPage().sendMailToSpam();
       Assert.assertTrue(mailbox.openInboxPage().isMailPresents()== false);
       Assert.assertTrue(mailbox.openSpamPage().isMailPresents(), "There is no mail on the page");
    }

}
