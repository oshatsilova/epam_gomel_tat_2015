package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.pages.FailLoginPage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class UnSuccessLogIntoYandexMailTest extends MailTestsBase {

    @Test(description = "UnSuccess login to Yandex mail with incorrect password")
    public void unSuccess_login() {
        FailLoginPage failLoginPage = new MailLoginPage().open().loginWithWrongPassword();
        Assert.assertTrue(failLoginPage.isErrorMessagePresent());
    }
}
