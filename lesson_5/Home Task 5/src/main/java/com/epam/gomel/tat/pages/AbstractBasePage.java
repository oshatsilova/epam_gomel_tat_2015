package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.UI.Browser;
import com.epam.gomel.tat.UI.Mail;
import com.epam.gomel.tat.UI.User;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public abstract class AbstractBasePage{

    protected Browser browser;
    protected Mail currentMail;
    protected User currentUser;

    public AbstractBasePage() {
        this.browser = Browser.get();
        this.currentUser = User.get();
        this.currentMail = Mail.get();
    }

}
