package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class FailLoginPage extends AbstractBasePage {

    private static final String errorMessage = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public Boolean isErrorMessagePresent() {
        return browser.isTextPresent(errorMessage);
    }
}
