package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailboxBasePage extends AbstractBasePage {

    public static final By MAIL_ACCOUNT_LINK_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#sent']");
    private static final By TRESH_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#trash']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#spam']");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage(getMailAccountLinkName());
    }
//оптимизировать всё это в один метод опен пэйдж принимающий к кач парам идентификатор страницы

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        return new MailSentListPage();
    }

    public MailTrashListPage openTreshPage() {
        browser.click(TRESH_LINK_LOCATOR);
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(SPAM_LINK_LOCATOR);
        return new MailSpamListPage();
    }

    public String getMailAccountLinkName() {
        return browser.getElementText(MAIL_ACCOUNT_LINK_LOCATOR);
    }


}
