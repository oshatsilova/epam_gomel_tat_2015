package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.FileUtils;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class ComposeMailPage extends AbstractBasePage {
    private static final By MAIL_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By MAIL_SUBJECT_INPUT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_INPUT_LOCATOR = By.id("compose-send");
    private static final By MAIL_SEND_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By MAIL_ATTACHE_INPUT_LOCATOR = By.xpath("//input[@name='att']");

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        return sendMail(mailTo, mailSubject, mailContent);
    }

    public MailboxBasePage sendRandomMail(Boolean withAttachment) {
        browser.type(MAIL_TO_INPUT_LOCATOR, currentMail.mailToAddress);
        browser.type(MAIL_SUBJECT_INPUT_LOCATOR, currentMail.mailSubject);
        browser.type(MAIL_TEXT_INPUT_LOCATOR, currentMail.mailContent);
        if (withAttachment) {
            FileUtils file = new FileUtils();
            file.format(currentMail.pathToFile);
            browser.attachFile(MAIL_ATTACHE_INPUT_LOCATOR, currentMail.pathToFile);
        }
        browser.click(MAIL_SEND_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        // wait for message
        return new MailboxBasePage();
    }
}
