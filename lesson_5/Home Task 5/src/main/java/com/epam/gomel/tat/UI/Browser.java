package com.epam.gomel.tat.UI;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class Browser {

    private static final String DOWNLOAD_DIR = "c:\\RES\\";
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    public static final int WAIT_ELEMENT_TIMEOUT = 20;
    private static final int AJAX_TIMEOUT = 20;

    private WebDriver driver;
    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    public static Browser init() {
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
        return new Browser(driver);
    }

    public void open(String URL) {
        driver.get(URL);
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void type(By locator, String text) {
        WebElement mailTextInput = driver.findElement(locator);
        mailTextInput.clear();
        mailTextInput.sendKeys(text);
    }

    public void attachFile(By locator, String path) {
        driver.findElement(locator).sendKeys(path);
    }

    public void waitForElementPresent(final By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isElementPresent(locator);
            }
        });
    }

    public void waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    public WebElement getElement(By locator) {
        return driver.findElement(locator);
    }

    public boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public String getElementText(By locator) {
        return driver.findElement(locator).getText();
    }


    public void close() {
        driver.quit();
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void submit(By locator) {
        driver.findElement(locator).submit();
    }

    public void checkMail(By locator) {
        driver.findElement(locator).click();
    }


    public boolean isTextPresent(String text) {
        try {
            WebElement content = driver.findElement(By.tagName("body"));
            content.getText().contains(text);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    public void waitAndClick(By locator) {
        waitForElementIsVisible(locator);
        click(locator);
    }
}

