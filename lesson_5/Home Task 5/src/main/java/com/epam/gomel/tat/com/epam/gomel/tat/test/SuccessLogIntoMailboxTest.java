package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.UI.User;
import com.epam.gomel.tat.pages.AbstractBasePage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 12.03.2015.
 */
public class SuccessLogIntoMailboxTest extends MailTestsBase {

   // private String correctUserLogin = "shatsilova@yandex.ru"; // don't forget to change to test data
    //private String correctUserPassword = "00223344"; // ACCOUNT

    @Test(description = "Success mail login")
    public void successLogin() {
        MailboxBasePage mailboxPage = new MailLoginPage().open().loginWithCorrectCredentials();
        Assert.assertEquals(mailboxPage.getMailAccountLinkName(), "oshatsilova@yandex.ru");
    }
}
