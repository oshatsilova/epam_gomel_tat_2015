package com.epam.gomel.tat.UI;

/**
 * Created by oshatsilova on 21.03.2015.
 */
public class Mail {
    public String mailToAddress;
    public String mailSubject;
    public String mailContent;
    public String pathToFile;
    private static Mail instance = null;

    public static Mail get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    public static Mail init() {
        Mail currentMail = new Mail();
        currentMail.mailToAddress = "shatsilova@yandex.ru";
        currentMail.mailSubject = "test mail subject: " + Math.random() * 100000000;
        currentMail.mailContent = "mail content: " + Math.random() * 100000000;
        currentMail.pathToFile = "C:\\TestAttachment.txt";
        return currentMail;
    }
}
