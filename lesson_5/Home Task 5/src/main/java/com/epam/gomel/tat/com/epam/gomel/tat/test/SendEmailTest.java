package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.pages.AbstractBasePage;
import com.epam.gomel.tat.pages.ComposeMailPage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class SendEmailTest extends MailTestsBase {

   @Test(description = "Success send email")
    public void sendEmail() {
        MailboxBasePage mailbox = new MailLoginPage()
                .open()
                .loginWithCorrectCredentials()
                .openInboxPage()
                .openComposeMailPage()
                .sendRandomMail(false); // is mail with attachment or not
        Assert.assertTrue(mailbox.openInboxPage().isMailPresents(), "There is no mail on the page");
        Assert.assertTrue(mailbox.openSentPage().isMailPresents(), "There is no mail on the page");
    }

}
