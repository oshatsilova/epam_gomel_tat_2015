package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailInboxListPage extends AbstractBasePage {
    String CURRENT_MAIL_SUBJECT_LOCATOR;

    public MailInboxListPage(String email) {
        CURRENT_MAIL_SUBJECT_LOCATOR = MAIL_SUBJECT_LOCATOR.replace("userEmeil", email);
    }

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final String MAIL_SUBJECT_LOCATOR = "//span[@class='js-messages-title-dropdown-name'][.='userEmeil']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']";
    private static final By INBOX_PAGE_CHECKBOX_LOCATOR = By.xpath("//input[@class='b-messages__message__checkbox__input']");
    private static final By DELETE_EMAIL_BUTTON = By.xpath("//img[@class='b-ico b-ico_delete']");
    private static final By MAIL_TO_SPAM_BUTTON = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='tospam']");

    public boolean isMailPresents() {
        CURRENT_MAIL_SUBJECT_LOCATOR = CURRENT_MAIL_SUBJECT_LOCATOR+"[.='"+currentMail.mailSubject+"']";//span[@class='js-messages-title-dropdown-name'][.='shatsilova@yandex.ru']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject'][.='test mail subject: 2.585841143760098E7']"
        return browser.isElementPresent(By.xpath(CURRENT_MAIL_SUBJECT_LOCATOR));
    }

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public void deleteMail() {
        chekMail();
        browser.waitForElementIsVisible(DELETE_EMAIL_BUTTON);
        browser.click(DELETE_EMAIL_BUTTON);
        browser.waitForAjaxProcessed();
    }

    private void chekMail() {
        browser.waitForElementIsVisible(INBOX_PAGE_CHECKBOX_LOCATOR);
        browser.click(INBOX_PAGE_CHECKBOX_LOCATOR); //check mail
    }

    public void sendMailToSpam() {
        chekMail();
        browser.waitForElementIsVisible(MAIL_TO_SPAM_BUTTON);
        browser.click(MAIL_TO_SPAM_BUTTON);
        browser.waitForAjaxProcessed();
    }
    public MailPage openMail() {
        CURRENT_MAIL_SUBJECT_LOCATOR = CURRENT_MAIL_SUBJECT_LOCATOR + "[.='" + currentMail.mailSubject + "']";//span[@class='js-messages-title-dropdown-name'][.='shatsilova@yandex.ru']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject'][.='test mail subject: 2.585841143760098E7']"
        browser.click(By.xpath(CURRENT_MAIL_SUBJECT_LOCATOR));
        return new MailPage();
    }

}
