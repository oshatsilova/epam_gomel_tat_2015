package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailSentListPage extends AbstractBasePage {
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMailPresents() {
        browser.waitForElementIsVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, currentMail.mailSubject)));
        return true; 
    }

}
