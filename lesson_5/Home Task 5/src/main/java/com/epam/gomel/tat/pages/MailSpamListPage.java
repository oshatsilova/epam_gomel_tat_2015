package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 21.03.2015.
 */
public class MailSpamListPage extends AbstractBasePage {
    private static final String MAIL_LINK_SUBJECT_LOCATOR = "//label[.='Спам']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']";
    private static final By SPAM_PAGE_CHECKBOX_LOCATOR = By.xpath("//label[.='Спам']/ancestor::*[@class='block-messages']//input[@class='b-messages__message__checkbox__input']");
    private static final By UNSPAM_MAIL_BUTTON = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='notspam']");

    public boolean isMailPresents() {
        String CURRENT_MAIL_LINK_SUBJECT_LOCATOR = MAIL_LINK_SUBJECT_LOCATOR+"[.='"+currentMail.mailSubject+"']";//span[@class='js-messages-title-dropdown-name'][.='shatsilova@yandex.ru']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject'][.='test mail subject: 2.585841143760098E7']"
        return browser.isElementPresent(By.xpath(CURRENT_MAIL_LINK_SUBJECT_LOCATOR));
        //browser.waitForElementIsVisible(By.xpath(String.format(MAIL_LINK_SUBJECT_LOCATOR, subject)));
        //return true;
    }
    private void chekMail() {
        browser.waitForElementIsVisible(SPAM_PAGE_CHECKBOX_LOCATOR);
        browser.click(SPAM_PAGE_CHECKBOX_LOCATOR); //check mail
    }

    public void unSpamMail() {
        chekMail();
        browser.waitForElementIsVisible(UNSPAM_MAIL_BUTTON);
        browser.click(UNSPAM_MAIL_BUTTON);
        browser.waitForAjaxProcessed();
    }
}
