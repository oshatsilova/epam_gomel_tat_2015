package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.pages.AbstractBasePage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class SendEmailWithAttachmentTest extends MailTestsBase {

     @Test(description = "Send email with attachment")
    public void sendEmailWithAttachment() {
         MailboxBasePage mailbox = new MailLoginPage()
                 .open()
                 .loginWithCorrectCredentials()
                 .openInboxPage()
                 .openComposeMailPage()
                 .sendRandomMail(true); // is mail with attachment or not
         Assert.assertTrue(mailbox.openSentPage().isMailPresents(), "There is no mail on the page");
         MailPage mailPage = mailbox.openInboxPage().openMail();
         mailPage.downloadAttachment();
         mailPage.verifyDownloadAttachment();
    }
}
