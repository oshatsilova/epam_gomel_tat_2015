package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.utils.FileUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

/**
 * Created by oshatsilova on 22.03.2015.
 */
public class MailPage extends AbstractBasePage {
    private static String DOWNLOAD_ATTACHMENT = "//span[@class='js-message-subject js-invalid-drag-target'][.='mailSubject']/ancestor::div[@class='b-layout__inner b-layout__inner_type_message js-message-inner']//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']";

    public void downloadAttachment() {
        DOWNLOAD_ATTACHMENT = DOWNLOAD_ATTACHMENT.replace("mailSubject", currentMail.mailSubject);
        browser.waitAndClick(By.xpath(DOWNLOAD_ATTACHMENT));
    }
    public void verifyDownloadAttachment() {
        FileUtils file = new FileUtils();
        Assert.assertTrue(file.compareFiles());
    }

}
