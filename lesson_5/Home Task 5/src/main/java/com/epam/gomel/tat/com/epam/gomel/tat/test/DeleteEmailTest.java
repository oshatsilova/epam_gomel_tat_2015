package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.pages.AbstractBasePage;
import com.epam.gomel.tat.pages.MailLoginPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 14.03.2015.
 */
public class DeleteEmailTest extends MailTestsBase {

    @Test(description = "Create new mail and delete it")
    public void deleteEmail() {
        MailboxBasePage mailbox = new MailLoginPage()
                .open()
                .loginWithCorrectCredentials()
                .openInboxPage()
                .openComposeMailPage()
                .sendRandomMail(false); // is mail with attachment or not
        mailbox.openInboxPage().deleteMail();
        Assert.assertTrue(mailbox.openInboxPage().isMailPresents()== false, "There is mail on the page");
        Assert.assertTrue(mailbox.openTreshPage().isMailPresents(), "There is no mail on the page");
    }

}
