package com.epam.gomel.tat.runner;

import com.epam.gomel.tat.reporting.CustomTestNgListener;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by oshatsilova on 26.03.2015.
 */
public class Runner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        for (String arg : args) {
            suites.add(arg);
        }
        testNG.setTestSuites(suites);
        testNG.addListener(new CustomTestNgListener());
        testNG.run();
    }
}

