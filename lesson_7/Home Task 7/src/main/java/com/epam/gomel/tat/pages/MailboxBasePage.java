package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.reporting.Logger;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailboxBasePage extends AbstractBasePage {

    public static final By MAIL_ACCOUNT_LINK_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#sent']");
    private static final By TRESH_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#trash']");
    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#spam']");

    public MailboxBasePage open() {
        browser.open(MailLoginPage.BASE_URL);
        return new MailboxBasePage();
    }

    public MailInboxListPage openInboxPage() {
        Logger.stepInfo("Open Inbox page");
        browser.waitForVisible(INBOX_LINK_LOCATOR);
        browser.click(INBOX_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        Logger.stepInfo("Open Sent page");
        browser.waitForVisible(SENT_LINK_LOCATOR);
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSentListPage();
    }

    public MailTrashListPage openTrashPage() {
        Logger.stepInfo("Open Trash page");
        browser.waitForVisible(TRESH_LINK_LOCATOR);
        browser.click(TRESH_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        Logger.stepInfo("Open Spam page");
        browser.waitForVisible(SPAM_LINK_LOCATOR);
        browser.click(SPAM_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public String getMailAccountLinkName() {
        return browser.getElementText(MAIL_ACCOUNT_LINK_LOCATOR);
    }


}
