package com.epam.gomel.tat.pages;

import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailTrashListPage extends AbstractBasePage {
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[.='Удалённые']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']";

    public boolean isMailPresents(String subject) {
        return browser.isElementPresent(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
    }
}
