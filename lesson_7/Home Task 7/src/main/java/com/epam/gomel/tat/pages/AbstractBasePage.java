package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.ui.Browser;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public abstract class AbstractBasePage {

    protected Browser browser;

    public AbstractBasePage() {
        this.browser = Browser.get();
    }
}
