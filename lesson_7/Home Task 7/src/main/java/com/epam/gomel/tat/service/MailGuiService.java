package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.exception.TestMailContentMatchException;
import com.epam.gomel.tat.pages.AbstractBasePage;
import com.epam.gomel.tat.pages.LetterPage;
import com.epam.gomel.tat.pages.MailInboxListPage;
import com.epam.gomel.tat.pages.MailboxBasePage;
import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.utils.FileTools;

/* Bad approach Service can't extends PageObject.
 * Also this cause problem with threads:
 * MailGuiService points to old browser, after first test, that instantiate MailGuiService.
 */
public class MailGuiService extends AbstractBasePage {

    public void sendMail(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage().open();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter);
    }

    public void checkMailPresentInboxList(Letter letter) {
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is in list on Inbox page");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        if (!mailbox.openInboxPage().isMailPresents(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is not on Inbox page list");
        }
    }

    public void checkMailNotPresentInboxList(Letter letter) {
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is absent in list on Inbox page");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        if (mailbox.openInboxPage().isMailPresents(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " there is on Inbox page list");
        }
    }

    public void checkMailPresentInSentList(Letter letter) {
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is in list on Sent page");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        if (!mailbox.openSentPage().isMailPresents(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is not on Inbox page list");
        }
    }

    public void checkMailPresentInTrashList(Letter letter) {
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is in list on Trash page");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        if (!mailbox.openTrashPage().isMailPresents(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is not on Trash page list");
        }
    }


    public void checkMailPresentInSpamList(Letter letter) {
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is in list on Spam page");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        if (!mailbox.openSpamPage().isMailPresents(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is not on Spam page list");
        }
    }

    public void checkMailNotPresentInSpamList(Letter letter) {
        Logger.stepInfo("Check mail with subject " + letter.getSubject() + " is absent in list on Spam page");
        MailboxBasePage mailbox = new MailboxBasePage().open();
        if (mailbox.openSpamPage().isMailPresents(letter.getSubject())) {
            throw new TestCommonRuntimeException("Mail with subject " + letter.getSubject() + " is on Spam page list");
        }
    }

    public void deleteMailI(Letter letter) {
        MailInboxListPage mailbox = new MailInboxListPage();
        mailbox.deleteMail(letter.getSubject());
    }

    public void sendMailToSpam(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().sendMailToSpam(letter.getSubject());
    }

    public void unSpamMail(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openSpamPage().unSpamMail(letter.getSubject());
    }

    public void checkMailContent(Letter letter) {
        LetterPage letterPage = new MailboxBasePage().open().openInboxPage().openLetter(letter.getSubject());
        // Should be Browser.get() or hidden into PageObject (better case)
        String receiver = browser.getElementText(letterPage.getRecipientMailLocator());
        String currentReceiver = receiver.substring(2, receiver.length() - 1);
        if (!currentReceiver.equals(letter.getReceiver())) {
            throw new TestMailContentMatchException("Receiver does not match to expected one: " + letter.getReceiver());
        }
        // Should be Browser.get() or hidden into PageObject (better case)
        if (!browser.getElementText(letterPage.getMailSubjectLocator()).equals(letter.getSubject())) {
            throw new TestMailContentMatchException("Subject does not match to expected one: " + letter.getSubject());
        }
        // Should be Browser.get() or hidden into PageObject (better case)
        if (!browser.getElementText(letterPage.getMailTextLocator()).equals(letter.getContent())) {
            throw new TestMailContentMatchException("Content does not match to expected one: " + letter.getContent());
        }
        if (letter.getAttach() != null) {
            try {
                letterPage.downloadAttachment(letter.getSubject());
                // Should be Browser.get() or hidden into PageObject (better case)
                if (!FileTools.compareFiles(letter.getAttach(), browser.getDownloadDir())) {
                    throw new TestMailContentMatchException("Attachment does not match to expected one: " + letter.getAttach());
                }
            } catch (Exception e) {
            }
        }

    }

}
