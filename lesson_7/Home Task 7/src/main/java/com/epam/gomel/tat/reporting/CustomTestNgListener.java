package com.epam.gomel.tat.reporting;

import com.epam.gomel.tat.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class CustomTestNgListener implements IResultListener2 {
    private String tab = "     "; // Should be final
    private static String tab2 = "          "; // Should be final

    @Override
    public void onStart(ITestContext context) {
        Logger.info("START : " + context.getCurrentXmlTest().getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        Logger.info("FINISH : " + context.getName());
        Browser.get().close();
    }

    @Override
    public void onTestStart(ITestResult result) {
        Logger.info(tab + "TEST METHOD START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Logger.info(tab + "TEST METHOD SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Logger.info(tab + "TEST METHOD FAILED: " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error(tab + "Test failed", result.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        Logger.info(tab + "TEST METHOD SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        Logger.info("TEST METHOD FAILED: " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error("Test method failed", result.getThrowable());
    }

    @Override
    public void beforeConfiguration(ITestResult result) {
        Logger.info("CONFIG START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        Logger.info("CONFIG SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        Logger.info("CONFIG FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error("Config failed", result.getThrowable());
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        Logger.info("CONFIG SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }
}
