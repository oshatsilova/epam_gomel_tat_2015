package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.reporting.Logger;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailSentListPage extends AbstractBasePage {
    private static String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//span[@class='b-messages__subject']";

    public boolean isMailPresents(String subject) {
        Logger.stepInfo("Verivy is mail presents on Sent page");
        return browser.isElementPresent(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
    }

}
