package com.epam.gomel.tat.runner;

import com.epam.gomel.tat.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.util.List;

/**
 * Created by oshatsilova on 3/30/2015.
 */
public class GlobalConfig {

    private static GlobalConfig instance;

    public static GlobalConfig instance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    @Option(name = "-bt", usage = "browser type", required = true)
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-tc", usage = "thread count")
    private int threadCount = 1;

    @Option(name = "-mode", usage = "parallel mode classes")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-suites_XML", handler = StringArrayOptionHandler.class)
    private List<String> suites;

    @Option(name = "-address", usage = "address")
    private String address = "http://localhost:";

    @Option(name = "-port", usage = "port")
    private int port = 4444;

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
