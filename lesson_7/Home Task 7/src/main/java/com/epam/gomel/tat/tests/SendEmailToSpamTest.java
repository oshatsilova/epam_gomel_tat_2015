package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import com.epam.gomel.tat.service.MailGuiService;
import com.epam.gomel.tat.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 15.03.2015.
 */
public class SendEmailToSpamTest extends  ParallelTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getDefaultLetter();

    @BeforeClass(description = "Login to account and send mail without attachment which will be send to spam")
    public void loginAndSandMail() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Send mail to spam")
    public void mailToSpam() {
        mailGuiService.sendMailToSpam(letter);
    }

    @Test(description = "Check mail is in inbox list", dependsOnMethods = "mailToSpam")
    public void checkMailNotInInboxtList() {
        mailGuiService.checkMailNotPresentInboxList(letter);
    }

    @Test(description = "Check mail is in inbox list", dependsOnMethods = "mailToSpam")
    public void checkMailInSpamList() {
        mailGuiService.checkMailPresentInSpamList(letter);
    }

}
