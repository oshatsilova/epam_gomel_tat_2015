package com.epam.gomel.tat.pages;

import org.openqa.selenium.By;

import java.io.IOException;

/**
 * Created by oshatsilova on 22.03.2015.
 */
public class LetterPage extends AbstractBasePage {
    private static final By RECIPIENT_MAIL_LOCATOR = By.xpath("//span[@class='b-message-head__email']");
    private static final By MAIL_SUBJECT_LOCATOR = By.xpath("//span[@class='js-message-subject js-invalid-drag-target']");
    private static final By MAIL_TEXT_INPUT_LOCATOR= By.xpath("//div[@class='b-message-body__content']/p");
    private static String DOWNLOAD_ATTACHMENT = "//span[@class='js-message-subject js-invalid-drag-target'][.='mailSubject']/ancestor::div[@class='b-layout__inner b-layout__inner_type_message js-message-inner']//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']";

    public void downloadAttachment(String path) throws IOException {
        DOWNLOAD_ATTACHMENT = DOWNLOAD_ATTACHMENT.replace("mailSubject", path);
        browser.waitAndClick(By.xpath(DOWNLOAD_ATTACHMENT));
    }
    public By getRecipientMailLocator() {
        return RECIPIENT_MAIL_LOCATOR;
    }
    public By getMailSubjectLocator() {
        return MAIL_SUBJECT_LOCATOR;
    }
    public By getMailTextLocator() {
        return MAIL_TEXT_INPUT_LOCATOR;
    }
}