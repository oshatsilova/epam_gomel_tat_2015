package com.epam.gomel.tat.pages;

import com.epam.gomel.tat.reporting.Logger;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class MailInboxListPage extends AbstractBasePage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final String INBOX_PAGE_CHECKBOX_LOCATOR = "//span[@class='b-messages__subject'][.='mailSubject']/ancestor::*[@data-action='mail.message.show-or-select']//input[@class='b-messages__message__checkbox__input']";
    private static final By MAIL_TO_SPAM_BUTTON = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='tospam']");
    private static final By DELETE_EMAIL_BUTTON = By.xpath("//img[@class='b-ico b-ico_delete']");
    // I know, next locator with hard cod user name, but i cant implement Account.getEmail method to insert it instead of ...
    private static final String MAIL_SUBJECT_LOCATOR = "//span[@class='js-messages-title-dropdown-name'][.='shatsilova@yandex.ru']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']";


    public ComposeMailPage openComposeMailPage() {
        Logger.stepInfo("Open ComposeMailPage");
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public boolean isMailPresents(String subject) {
        Logger.stepInfo("Verify is mail with subject: " + subject + " presents on Inbox page");
        String CURRENT_MAIL_SUBJECT_LOCATOR = MAIL_SUBJECT_LOCATOR + "[.='" + subject + "']";
        return browser.isElementPresent(By.xpath(CURRENT_MAIL_SUBJECT_LOCATOR));
    }

    public LetterPage openLetter(String subject) {
        Logger.stepInfo("Open letter with subject: " + subject);
        String CURRENT_MAIL_SUBJECT_LOCATOR = MAIL_SUBJECT_LOCATOR + "[.='" + subject + "']";//span[@class='js-messages-title-dropdown-name'][.='shatsilova@yandex.ru']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject'][.='test mail subject: 2.585841143760098E7']"
        browser.click(By.xpath(CURRENT_MAIL_SUBJECT_LOCATOR));
        return new LetterPage();
    }

    public void deleteMail(String subject) {
        Logger.stepInfo("Delete mail with subject: " + subject);
        String CHECKBOX_LOCATOR = INBOX_PAGE_CHECKBOX_LOCATOR.replace("mailSubject", subject);
        browser.waitAndClick(By.xpath(CHECKBOX_LOCATOR));
        browser.waitAndClick(DELETE_EMAIL_BUTTON);
        browser.waitForAjaxProcessed();
    }

    public void sendMailToSpam(String subject) {
        Logger.stepInfo("Send mail with subject: " + subject + " to Spam");
        String CHECKBOX_LOCATOR = INBOX_PAGE_CHECKBOX_LOCATOR.replace("mailSubject", subject);
        browser.waitAndClick(By.xpath(CHECKBOX_LOCATOR));
        browser.waitAndClick(MAIL_TO_SPAM_BUTTON);
        browser.waitForAjaxProcessed();
    }

}
