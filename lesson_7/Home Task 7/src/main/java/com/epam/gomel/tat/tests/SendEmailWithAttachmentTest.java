package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import com.epam.gomel.tat.service.MailGuiService;
import com.epam.gomel.tat.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class SendEmailWithAttachmentTest  extends  ParallelTest{
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private Letter letter;

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send random mail with attachment")
    public void sendMailWithAttachment() throws IOException {
        letter = LetterBuilder.getLetterWithAttach();
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail is in inbox list", dependsOnMethods = "sendMailWithAttachment")
    public void checkMailInInboxtList() {
        mailGuiService.checkMailPresentInboxList(letter);
    }

    @Test(description = "Check received mail content", dependsOnMethods = "checkMailInInboxtList")
    public void checkMailContent() {
        mailGuiService.checkMailContent(letter);
    }

    @Test(description = "Check mail is in sent list", dependsOnMethods = "sendMailWithAttachment")
    public void checkMailInSentList() {
        mailGuiService.checkMailPresentInSentList(letter);
    }

}
