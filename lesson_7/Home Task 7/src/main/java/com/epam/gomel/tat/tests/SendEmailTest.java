package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import com.epam.gomel.tat.service.MailGuiService;
import com.epam.gomel.tat.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class SendEmailTest  extends  ParallelTest{
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getDefaultLetter();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send random mail without attachment")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail is in inbox list", dependsOnMethods = "sendMail")
    public void checkMailIsInInboxtList() {
        mailGuiService.checkMailPresentInboxList(letter);
    }

    @Test(description = "Check received mail content", dependsOnMethods = "checkMailIsInInboxtList")
    public void checkMailContent() {
        mailGuiService.checkMailContent(letter);
    }

    @Test(description = "Check mail is in sent list", dependsOnMethods = "sendMail")
    public void checkMailIsInSentList() {
        mailGuiService.checkMailPresentInSentList(letter);
    }

}