package com.epam.gomel.tat.lesson_6.runner;

import com.epam.gomel.tat.lesson_6.reporting.CustomTestNgListener;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    // TODO
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        testNG.addListener(new CustomTestNgListener());
        suites.add("target/classes/suites/send_mail_suit.xml");
        testNG.setTestSuites(suites);
        testNG.run();
    }

}
