package com.epam.gomel.tat.reporting;

public class Logger {

    private static org.apache.log4j.Logger My_log = org.apache.log4j.Logger.getLogger(Logger.class);
    private static String tab2 = "          ";


    public static void error(String s) {
        My_log.error(s);
    }

    public static void error(String s, Throwable e) {
        My_log.error(s, e);
    }

    public static void fatal(String s) {
        My_log.fatal(s);
    }

    public static void fatal(String s, Throwable t) {
        My_log.fatal(s, t);
    }

    public static void warn(String s) {
        My_log.warn(s);
    }

    public static void warn(String s, Throwable t) {
        My_log.warn(s, t);
    }

    public static void info(String s) {
        My_log.info(s);
    }

    public static void stepInfo(String s) {
        My_log.info(tab2 + s);
    }

    public static void info(String s, Throwable t) {
        My_log.info(s, t);
    }

    public static void debug(String s) {
        My_log.debug(s);
    }

    public static void debug(String s, Throwable t) {
        My_log.debug(s, t);
    }

    public static void trace(String s) {
        My_log.trace(s);
    }

    public static void trace(String s, Throwable t) {
        My_log.trace(s, t);
    }
}
