package com.epam.gomel.tat.pages;

import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 21.03.2015.
 */
public class MailSpamListPage extends AbstractBasePage {
    private static final String MAIL_SUBJECT_LOCATOR = "//label[.='Спам']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']";
    private static final String SPAM_PAGE_CHECKBOX_LOCATOR = "//label[.='Спам']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject'][.='mailSubject']/ancestor::*[@data-action='mail.message.show-or-select']//input[@class='b-messages__message__checkbox__input']";
    private static final By UNSPAM_MAIL_BUTTON = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='notspam']");

    public boolean isMailPresents(String subject) {
        String CURRENT_MAIL_SUBJECT_LOCATOR = MAIL_SUBJECT_LOCATOR + "[.='" + subject + "']";
        return browser.isElementPresent(By.xpath(CURRENT_MAIL_SUBJECT_LOCATOR));
    }

    public void unSpamMail(String subject) {
        String CURRENT_SPAM_PAGE_CHECKBOX_LOCATOR = SPAM_PAGE_CHECKBOX_LOCATOR.replace("mailSubject", subject);
        browser.waitAndClick(By.xpath(CURRENT_SPAM_PAGE_CHECKBOX_LOCATOR));
        browser.waitForVisible(UNSPAM_MAIL_BUTTON);
        browser.click(UNSPAM_MAIL_BUTTON);
        browser.waitForAjaxProcessed();
    }
}
