package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.MailGuiService;
import com.epam.gomel.tat.service.LoginGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 14.03.2015.
 */
public class DeleteEmailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getDefaultLetter();

    @BeforeClass(description = "Login to account and send mail without attachment which will deleted later")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Delete mail")
    public void deleteMail() {
        mailGuiService.deleteMailI(letter);
    }

    @Test(description = "Check mail is not in inbox list", dependsOnMethods = "deleteMail")
    public void checkMailNotInInboxList() {
        mailGuiService.checkMailNotPresentInboxList(letter);
    }

    @Test(description = "Check mail is in trash list", dependsOnMethods = "deleteMail")
    public void checkMailInTrashList() {
        mailGuiService.checkMailPresentInTrashList(letter);
    }

}
