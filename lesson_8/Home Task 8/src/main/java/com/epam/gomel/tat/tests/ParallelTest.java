package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.runner.GlobalConfig;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;

/**
 * Created by oshatsilova on 4/01/2015.
 */
public class ParallelTest extends BaseTest {

    @BeforeSuite
    public void befSuite(ITestContext context) {
        context.getSuite().getXmlSuite().setParallel(GlobalConfig.instance().getParallelMode().getAlias());
        context.getSuite().getXmlSuite().setThreadCount(GlobalConfig.instance().getThreadCount());
    }
}
