package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 12.03.2015.
 */
public class SuccessLogIntoMailboxTest extends ParallelTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @Test(description = "Success mail login")
    public void successLogin() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

}
