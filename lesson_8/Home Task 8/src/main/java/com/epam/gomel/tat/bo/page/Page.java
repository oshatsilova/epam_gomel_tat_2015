package com.epam.gomel.tat.bo.page;

import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 05.04.2015.
 */
public class Page {
    private String pageName;
    private By pageLinkLocator;

    public Page(Page page) {
        this.pageName = page.getPageName();
        this.pageLinkLocator = page.getPageLinkLocator();
    }

    public Page(String pageName, By pageLinkLocator) {
        this.pageName = pageName;
        this.pageLinkLocator = pageLinkLocator;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public By getPageLinkLocator() {
        return pageLinkLocator;
    }

    public void setPageLinkLocator(By pageLinkLocator) {
        this.pageLinkLocator = pageLinkLocator;
    }


}
