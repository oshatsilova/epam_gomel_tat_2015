package com.epam.gomel.tat.pages.YandexDiskPages;

import com.epam.gomel.tat.ui.Browser;
import com.epam.gomel.tat.Element;
import org.openqa.selenium.By;

import java.io.File;
import java.io.IOException;

/**
 * Created by oshatsilova on 06.04.2015.
 */
public class DiskBasePage {

    private Element diskAccountLinklocator = new Element(By.xpath("//span[@class='_nb-user-name _link']"));
    private Element uploadFileButtonLocator = new Element(By.xpath("//input [@class='_nb-file-intruder-input']"));
    public Element submitAttachFileLocator = new Element(By.xpath("//div[@class='_nb-popup-i']/a"));
    private Element recycleIconLocator = new Element(By.xpath("//div[@title='Корзина']"));
    private Element fileOnBasePageLocator = new Element("div.ns-view-listing.b-listing.b-listing_view_icons.b-listing_group-by_none.ns-view-visible div[title=\"%s\"]");
    public Element downloadButtonLocator = new Element(By.cssSelector("span.b-button__text"));

    public String getAccountLinkName() {
        return diskAccountLinklocator.waitForPresent().getText();
    }

    public DiskBasePage uploadFile(File file) throws IOException {
        uploadFileButtonLocator.waitForPresent().attachFile(file.getPath());
        submitAttachFileLocator.waitFoVisible().click();
        fileOnBasePageLocator.formatLocator(file.getName()).waitForPresent();
        return this;
    }

    public void downloadFile(String name) {
        String currentWindowHandler = Browser.get().getWindowHandler();
        fileOnBasePageLocator.formatLocator(name).waitForPresent().doubleClick();
        Browser.get().switchToNewWindow();
        downloadButtonLocator.waitForPresent().click();
        Browser.get().switchToWindow(currentWindowHandler);
    }

    public void removedFileToRecycle(String name) {
        fileOnBasePageLocator.formatLocator(name).waitForPresent().dragAndDropTo(recycleIconLocator.getLocator());
    }

    public DiskRecyclePage openRecyclePage() {
        recycleIconLocator.waitForPresent().doubleClick();
        return new DiskRecyclePage();
    }

    public boolean isFileOnBasePage(String name) {
        return fileOnBasePageLocator.formatLocator(name).isPresent();
    }
}
