package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.page.Page;
import com.epam.gomel.tat.bo.page.PageBuilder;
import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.LoginGuiService;
import com.epam.gomel.tat.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 15.03.2015.
 */
public class UnSpamEmailTest extends ParallelTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getDefaultLetter();
    private Page inboxPage = new Page(PageBuilder.getInboxPage());
    private Page spamPage = new Page(PageBuilder.getSpamPage());

    @BeforeClass(description = "Login to account, send mail without attachment and send it to spam")
    public void loginSendAndSpam() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
        mailGuiService.sendMail(letter);
        mailGuiService.sendMailToSpam(letter);
    }

    @Test(description = "Unspam mail")
    public void unSpamMail() {
        mailGuiService.unSpamMail(letter);
    }

    @Test(description = "Check mail is in inbox list", dependsOnMethods = "unSpamMail")
    public void checkMailIsInInboxtList() {
        mailGuiService.checkMailPresentOnPage(letter, inboxPage);
    }

    @Test(description = "Check mail is not in spam list", dependsOnMethods = "unSpamMail")
    public void checkMailNotInSpamList() {
        mailGuiService.checkMailNotPresentOnPage(letter, spamPage);
    }

}
