package com.epam.gomel.tat.tests.yandexDisk;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.YandexDiskService;
import com.epam.gomel.tat.tests.ParallelTest;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 12.03.2015.
 */
public class LoginToYandexDiskTest extends ParallelTest {

    private YandexDiskService yandexDiskService = new YandexDiskService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to Yandex disk")
    public void loginToYandex() {
        yandexDiskService.loginToDisk(defaultAccount);
    }

}
