package com.epam.gomel.tat.tests.yandexDisk;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.YandexDiskService;
import com.epam.gomel.tat.tests.ParallelTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by oshatsilova on 07.04.2015.
 */
public class RecoverFileFromRecycle extends ParallelTest{
    private YandexDiskService yandexDiskService = new YandexDiskService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private File file;;

    @BeforeClass(description = "Login to yandex disk,upload the file and remove it to recycle")
    public void loginUploadAndRemoveFile() throws Exception{
        yandexDiskService.loginToDisk(defaultAccount);
        yandexDiskService.uploadFileToDisk();
        yandexDiskService.removeFileFromDisk();
        yandexDiskService.checkFileRemoved();
    }
    @Test(description = "Recover file from recycle")
    public void recoverFile() {
        yandexDiskService.recoverFileFromRecycle();
    }

    @Test(description = "Check file is recovered", dependsOnMethods = "recoverFile")
    public void checkFileIsOnDisk() throws Exception {
        yandexDiskService.checkFilOnDisk();
    }
}
