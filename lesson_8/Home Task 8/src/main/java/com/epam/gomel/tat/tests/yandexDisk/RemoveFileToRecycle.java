package com.epam.gomel.tat.tests.yandexDisk;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.YandexDiskService;
import com.epam.gomel.tat.tests.ParallelTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by oshatsilova on 07.04.2015.
 */
public class RemoveFileToRecycle extends ParallelTest {

    private YandexDiskService yandexDiskService = new YandexDiskService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private File file;;

    @BeforeClass(description = "Login to yandex disk and upload the file")
    public void loginToYandexDiskAndUploadFile() throws Exception{
        yandexDiskService.loginToDisk(defaultAccount);
        yandexDiskService.uploadFileToDisk();
    }

    @Test(description = "Remove file to recycle")
    public void removeFile() {
        yandexDiskService.removeFileFromDisk();
    }

    @Test(description = "Check file is removed", dependsOnMethods = "removeFile")
    public void checkFileRemoved() throws Exception {
        yandexDiskService.checkFileRemoved();
    }
}
