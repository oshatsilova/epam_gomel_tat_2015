package com.epam.gomel.tat.pages.MailServicePages;

import com.epam.gomel.tat.bo.page.PageBuilder;
import com.epam.gomel.tat.reporting.Logger;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class ActionsWithMail extends MailboxBasePage {

    private static final By MAIL_TO_SPAM_BUTTON = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='tospam']");
    private static final By DELETE_EMAIL_BUTTON = By.xpath("//img[@class='b-ico b-ico_delete']");
    private static final By UNSPAM_MAIL_BUTTON = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='notspam']");

    public void deleteMail(String subject) {
        new MailboxBasePage().openPage(PageBuilder.getInboxPage());
        Logger.stepInfo("Delete mail with subject: " + subject);
        selectMail(subject);
        browser.waitForVisible(DELETE_EMAIL_BUTTON);
        browser.click(DELETE_EMAIL_BUTTON);
        browser.waitForAjaxProcessed();
    }

    public void sendMailToSpam(String subject) {
        Logger.stepInfo("Send mail with subject: " + subject + " to Spam");
        selectMail(subject);
        browser.waitForVisible(MAIL_TO_SPAM_BUTTON);
        browser.click(MAIL_TO_SPAM_BUTTON);
        browser.waitForAjaxProcessed();
    }
    public void unSpamMail(String subject) {
        Logger.stepInfo("Unspam mail with subject: " + subject);
        selectMail(subject);
        browser.waitForVisible(UNSPAM_MAIL_BUTTON);
        browser.click(UNSPAM_MAIL_BUTTON);
        browser.waitForAjaxProcessed();
    }
}
