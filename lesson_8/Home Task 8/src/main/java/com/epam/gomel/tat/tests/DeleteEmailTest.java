package com.epam.gomel.tat.tests;

import com.epam.gomel.tat.bo.page.Page;
import com.epam.gomel.tat.bo.page.PageBuilder;
import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.bo.mail.LetterBuilder;
import com.epam.gomel.tat.service.MailGuiService;
import com.epam.gomel.tat.service.LoginGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 14.03.2015.
 */
public class DeleteEmailTest extends  ParallelTest{
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getDefaultLetter();
    private Page inboxPage = new Page(PageBuilder.getInboxPage());
    private  Page trashPage = new Page(PageBuilder.getTrashPage());

    @BeforeClass(description = "Login to account and send mail without attachment which will deleted later")
    public void loginToAccountAndSendMail() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Delete mail")
    public void deleteMail() {
        mailGuiService.deleteMailI(letter);
    }

    @Test(description = "Check mail is not in inbox list", dependsOnMethods = "deleteMail")
    public void checkMailNotInInboxList() {
        mailGuiService.checkMailNotPresentOnPage(letter, inboxPage);
    }

    @Test(description = "Check mail is in trash list", dependsOnMethods = "deleteMail")
    public void checkMailInTrashList() {
        mailGuiService.checkMailPresentOnPage(letter, trashPage);
    }

}
