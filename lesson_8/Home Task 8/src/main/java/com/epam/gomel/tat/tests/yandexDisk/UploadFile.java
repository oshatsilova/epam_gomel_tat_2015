package com.epam.gomel.tat.tests.yandexDisk;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.YandexDiskService;
import com.epam.gomel.tat.tests.ParallelTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;


/**
 * Created by oshatsilova on 06.04.2015.
 */
public class UploadFile extends ParallelTest {

    private YandexDiskService yandexDiskService = new YandexDiskService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to yandex disk")
    public void loginToYandexDisk() {
        yandexDiskService.loginToDisk(defaultAccount);
    }

    @Test(description = "Upload file")
    public void uploadFile() throws Exception {
        yandexDiskService.uploadFileToDisk();
    }

    @Test(description = "Check file is downloaded", dependsOnMethods = "uploadFile")
    public void checkFileIsOnDisk() throws Exception {
        yandexDiskService.checkFilOnDisk();
    }
}
