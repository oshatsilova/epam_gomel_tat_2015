package com.epam.gomel.tat.pages.MailServicePages;

import com.epam.gomel.tat.bo.mail.Letter;
import com.epam.gomel.tat.reporting.Logger;
import org.openqa.selenium.By;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class ComposeMailPage extends MailboxBasePage {
    private static final By MAIL_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By MAIL_SUBJECT_INPUT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_INPUT_LOCATOR = By.id("compose-send");
    private static final By MAIL_SEND_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By MAIL_ATTACHE_INPUT_LOCATOR = By.xpath("//input[@name='att']");

    public MailboxBasePage sendMail(Letter letter) {
        Logger.stepInfo("Send mail with subject: " + letter.getSubject());
        browser.waitForElementPresent(MAIL_TO_INPUT_LOCATOR);
        browser.type(MAIL_TO_INPUT_LOCATOR, letter.getReceiver());
        browser.waitForElementPresent(MAIL_SUBJECT_INPUT_LOCATOR);
        browser.type(MAIL_SUBJECT_INPUT_LOCATOR, letter.getSubject());
        browser.waitForElementPresent(MAIL_TEXT_INPUT_LOCATOR);
        browser.type(MAIL_TEXT_INPUT_LOCATOR, letter.getContent());
        if (letter.getAttach() != null) {
            browser.attachFile(MAIL_ATTACHE_INPUT_LOCATOR, letter.getAttach());
        }
        browser.click(MAIL_SEND_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

}
