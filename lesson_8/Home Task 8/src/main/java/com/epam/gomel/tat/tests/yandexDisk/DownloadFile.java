package com.epam.gomel.tat.tests.yandexDisk;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.bo.common.AccountBuilder;
import com.epam.gomel.tat.service.YandexDiskService;
import com.epam.gomel.tat.tests.ParallelTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 08.04.2015.
 */
public class DownloadFile extends ParallelTest {
    private YandexDiskService yandexDiskService = new YandexDiskService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to yandex disk")
    public void loginAndUploadFile() throws Exception {
        yandexDiskService.loginToDisk(defaultAccount);
        yandexDiskService.uploadFileToDisk();
    }

    @Test(description = "Download file")
    public void downloadFile() {
        yandexDiskService.downloadFileFromDisk();
    }

    @Test(description = "Check downloaded file", dependsOnMethods = "downloadFile")
    public void checkDownloadedFile() throws Exception {
        yandexDiskService.checkDownloadFile();
    }
}
