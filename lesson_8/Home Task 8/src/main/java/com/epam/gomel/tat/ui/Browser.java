package com.epam.gomel.tat.ui;

import com.epam.gomel.tat.reporting.Logger;
import com.google.common.base.Predicate;
import com.epam.gomel.tat.utils.FileTools;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.epam.gomel.tat.runner.GlobalConfig;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/**
 * Created by oshatsilova on 20.03.2015.
 */
public class Browser {

    public static final String DOWNLOAD_DIR = "R:\\Epam\\Rep\\epam_gomel_tat_2015\\lesson_8\\Home Task 8\\DOWNLOAD\\";//FileUtils.getTempDirectoryPath();
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT = 200;
    private static final int COMMAND_DEFAULT_TIMEOUT = 5;
    private static final int WAIT_ELEMENT_TIMEOUT = 20;
    private static final int AJAX_TIMEOUT = 20;
    private static final String address = GlobalConfig.instance().getAddress();
    private static final int port = GlobalConfig.instance().getPort();
    public static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private WebDriver driver;
    private boolean augmented;
    private static Map<Thread, Browser> instances = new HashMap<>();

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = GlobalConfig.instance().getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                try {
                    driver = new RemoteWebDriver(new URL(address + port + "/wd/hub"),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }

                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                //driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                //driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                //driver.manage().window().maximize();
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", FileTools.getCanonicalPathToResourceFile("resources\\chromedriver.exe"));
                driver = new ChromeDriver();
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                //driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case HTMLUNIT:
                DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
                driver = new HtmlUnitDriver(capabilities);
                ((HtmlUnitDriver) driver).setJavascriptEnabled(true);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                //driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT, TimeUnit.SECONDS);
                break;
        }
        Thread currentThread = Thread.currentThread();
        currentThread.setName("№" + Integer.toString(instances.size() + 1));
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String URL) {
        Logger.stepInfo("Open page: " + URL);
        driver.get(URL);
        instances.get(Thread.currentThread()).takeScreenshot();

    }

    public void close() {
        Logger.stepInfo("Close browser");
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();

            } catch (Exception e) {
                Logger.error("Cannot close browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void click(By locator) {
        elementHighlight(driver.findElement(locator)).click();
    }

    public void type(By locator, String text) {
        WebElement mailTextInput = driver.findElement(locator);
        mailTextInput.clear();
        elementHighlight(mailTextInput).sendKeys(text);
    }

    public void attachFile(By locator, String path) {

        waitForElementPresent(locator);
        driver.findElement(locator).sendKeys(path);
        instances.get(Thread.currentThread()).takeScreenshot();
    }

    public void waitForVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void waitForElementPresent(final By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isElementPresent(locator);
            }
        });  //куда добавить снитие скрина при ошибке
        // как получить имя элемента в лог
        // что за эксепшн в блоке рай / кэтч
        //как не ждать подгрузку всего контента страницы
    }

    public boolean isTextPresent(String text) {
        Logger.stepInfo("Verify is text \"" + text + "\" present");
        instances.get(Thread.currentThread()).takeScreenshot();
        try {
            driver.getPageSource().contains(text);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    public void waitForTextPresent(final String text) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isTextPresent(text);
            }
        });
    }
    public String getElementText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void takeScreenshot() {
        if (GlobalConfig.instance().getBrowserType() == BrowserType.REMOTE &&
                !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }

        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.stepInfo("Screenshot saved to local directory:  " + copy.getParent().toString() + "   as - " + copy.getName());
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot");
        }
    }

    public void dragAndDrop(By sourceLocator, By targetLocator) {
        WebElement draggable = driver.findElement(sourceLocator);
        WebElement target = driver.findElement(targetLocator);
        new Actions(driver).dragAndDrop(draggable, target).build().perform();
    }


    public void mouseOver(By locator) {
        WebElement element = driver.findElement(locator);
        new Actions(driver).moveToElement(element, 2, 2).build().perform();
    }

    public void doubleClick(By locator) {
        WebElement element = driver.findElement(locator);
        new Actions(driver).doubleClick(element).build().perform();
    }

    public void switchToNewWindow() {
        Logger.stepInfo("Switch to new window");
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    public void switchToWindow(String handler) {
        Logger.stepInfo("Switch to window^ " + handler);
        driver.switchTo().window(handler);
    }

    public WebElement elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
        return element;
    }

    public String getWindowHandler(){
        return driver.getWindowHandle();
    }
}
