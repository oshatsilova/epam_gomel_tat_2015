package com.epam.gomel.tat.service;

import com.epam.gomel.tat.bo.common.Account;
import com.epam.gomel.tat.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.pages.YandexDiskPages.DiskRecyclePage;
import com.epam.gomel.tat.reporting.Logger;
import com.epam.gomel.tat.pages.YandexDiskPages.DiskBasePage;
import com.epam.gomel.tat.pages.YandexDiskPages.DiskLoginPage;
import com.epam.gomel.tat.utils.FileTools;

import java.io.File;
import java.io.IOException;

/**
 * Created by oshatsilova on 06.04.2015.
 */
public class YandexDiskService {

    File uploadFile;

    public void loginToDisk(Account account) {
        Logger.stepInfo("Login to yandex disk as: " + account.getLogin());
        DiskBasePage diskPage = new DiskLoginPage().open().login(account.getLogin(), account.getPassword());
        checkDiskAccout(account, diskPage);
    }

    private void checkDiskAccout(Account account, DiskBasePage diskPage) {
        Logger.stepInfo("Check yandex disk account link");
        String userNick = diskPage.getAccountLinkName();
        if (userNick == null || !userNick.equals(account.getLogin())) {
            throw new TestCommonRuntimeException("Login failed. User nick: '" + userNick + "'");
        }
    }

    public void uploadFileToDisk() throws IOException {
        Logger.stepInfo("Upload rundom file to yandex disk");
        uploadFile = new File(FileTools.createRandomFile());
        new DiskBasePage().uploadFile(uploadFile);
    }

    public void downloadFileFromDisk() {
        Logger.stepInfo("Download file from yandex disk");
        new DiskBasePage().downloadFile(uploadFile.getName());
    }

    public void checkDownloadFile() throws IOException {
        Logger.stepInfo("Compare upload and downljad files");
        if (!FileTools.compareFiles(uploadFile.getPath())) {
            throw new TestCommonRuntimeException("Downloaded file didn't match to uploaded file");
        }
    }
    public void removeFileFromDisk() {
        Logger.stepInfo("Remove file from yandex disk to recycle");
        new DiskBasePage().removedFileToRecycle(uploadFile.getName());
    }
    public void recoverFileFromRecycle() {
        Logger.stepInfo("Recover file from recycle");
        new DiskRecyclePage().recoverFileFromRecycle(uploadFile.getName());
    }

    public void checkFileRemoved() throws IOException {
        Logger.stepInfo("Check removed file is in the recycle");
        if (!new DiskBasePage().openRecyclePage().isFileInRecycle(uploadFile.getName())) {
            throw new TestCommonRuntimeException("File "+uploadFile.getName()+" is absent in recycle");
        }
    }
    public void checkFilOnDisk() throws IOException {
        Logger.stepInfo("Check removed file is on base disk page");
        if (!new DiskBasePage().isFileOnBasePage(uploadFile.getName())) {
            throw new TestCommonRuntimeException("File "+uploadFile.getName()+" is absent on disk");
        }
    }
}

