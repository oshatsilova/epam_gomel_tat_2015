package com.epam.tat.lesson8.test;

import com.epam.tat.lesson8.page.GooglePage;
import com.epam.tat.lesson8.page.PriorHomePage;
import com.epam.tat.lesson8.utils.Logger;
import com.epam.tat.lesson8.utils.Timeout;
import org.testng.annotations.*;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class ExampleTest extends ParallelTest {

    @Test(description = "Try to open google page")
    @Parameters(value = "number")
    public void openPage(@Optional(value = "0") String number) {
        Logger.info("This test have try: " + number);
        GooglePage.open();
        Timeout.sleep(5);
    }

    @Test(description = "Perform login to Prior")
    public void performLoginToPrior() {
        PriorHomePage.open().login("user", "1q2w3e").checkAntiRobotWorks();
        Timeout.sleep(2);
    }
}
