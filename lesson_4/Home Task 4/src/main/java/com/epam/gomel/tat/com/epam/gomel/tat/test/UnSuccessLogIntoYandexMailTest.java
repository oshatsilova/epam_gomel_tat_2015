package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class UnSuccessLogIntoYandexMailTest extends MailTestsBase {

    @Test(description = "UnSuccess login to Yandex mail with incorrect password")
    public void unSuccess_login() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword + Integer.toString((int) (Math.random() * 100)));
        service.verifyMessage(service.ERROR_MAIL_LOGIN_MESSAGE_LOCATOR, service.errorMailLoginMessage);
    }
}
