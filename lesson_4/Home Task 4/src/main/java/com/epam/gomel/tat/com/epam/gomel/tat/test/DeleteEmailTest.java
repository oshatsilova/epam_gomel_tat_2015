package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 14.03.2015.
 */
public class DeleteEmailTest extends MailTestsBase {

    @Test(description = "Delete email")
    public void deleteEmail() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword);
        service.sendRandomEmailTo(service.mailToAddress, false);
        service.waitForElementPresent(service.DONE_LOCATOR).click();
        service.waitForElementPresent(service.INBOX_LINK_LOCATOR).click();
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_INBOX_PAGE, this.service.mailSubject);
        service.markFirstEmailOnPage(service.INBOX_PAGE_CHECKBOX_LOCATOR);
        service.waitForElementVisible(service.DELETE_EMAIL_LOCATOR).click();
        //Добавить проверку того что письмо отсутствует во входящих - удаляй коменты (особенно на русском) когда будешь делать final pull request
        service.goToPage(service.TRESH_LINK_LOCATOR);
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_DELETED_PAGE, this.service.mailSubject);
    }

}
