package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class SuccessSendEmailTest extends MailTestsBase {
    @Test(description = "Success send email")
    public void sendEmail() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword);
        service.sendRandomEmailTo(service.mailToAddress, false);
        service.waitForElementPresent(service.DONE_LOCATOR).click();
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_INBOX_PAGE, this.service.mailSubject);
        service.goToPage(service.SENT_LINK_LOCATOR);
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_SENT_PAGE, this.service.mailSubject);
    }

}
