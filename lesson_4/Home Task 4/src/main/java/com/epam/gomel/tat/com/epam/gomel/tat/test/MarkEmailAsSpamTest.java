package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 15.03.2015.
 */
public class MarkEmailAsSpamTest extends MailTestsBase {
    @Test(description = "Mark email as a spam")
    public void markEmailAsSpam() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword);
        service.sendRandomEmailTo(service.mailToAddress, false);
        service.waitForElementPresent(service.DONE_LOCATOR).click();
        service.waitForElementPresent(service.INBOX_LINK_LOCATOR).click();
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_INBOX_PAGE, this.service.mailSubject);
        service.markFirstEmailOnPage(service.INBOX_PAGE_CHECKBOX_LOCATOR);
        service.waitForElementPresent(service.SPAM_ICON_LOCATOR).click();
        //Добавить проверку того что письмо отсутствует во входящих
        service.goToPage(service.SPAM_LINK_LOCATOR);
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_SPAM_PAGE, this.service.mailSubject);
    }
}
