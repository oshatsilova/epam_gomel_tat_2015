package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 12.03.2015.
 */
public class SuccessLogIntoYandexMailTest extends MailTestsBase { // можно было удалить Yandex из имени

    @Test(description = "Success mail login")
    public void Success_login() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword);
        service.verifyMailAccount();
    }
}
