package com.epam.gomel.tat.com.epam.gomel.tat.test;

//import org.testng.Assert;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 13.03.2015.
 */
public class SuccessSendEmailWithAttachmentTest extends MailTestsBase {
    @Test(description = "Success send email with attachment")
    public void sendEmailWithAttachment() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword);
        service.sendRandomEmailTo(service.mailToAddress, true);
        service.waitForElementPresent(service.DONE_LOCATOR).click();
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_INBOX_PAGE, this.service.mailSubject);
        /*Скачать файл для сравнения так и не удалось, по этому
        * пока сравниваю с предварительно сохранённым прототипом*/
        String downloadedAttachmentContext = file.open(service.pathToFile);
        Assert.assertEquals(downloadedAttachmentContext, this.service.mailContent, "Attachment does not match the sent attachment");
    }
}
