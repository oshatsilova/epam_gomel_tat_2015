package com.epam.gomel.tat.com.epam.gomel.tat.test;

import com.epam.gomel.tat.utils.MailTestsBase;
import org.testng.annotations.Test;

/**
 * Created by oshatsilova on 15.03.2015.
 */
public class MarkEmailAsNotSpamTest extends MailTestsBase {
    @Test(description = "Mark email as not spam")
    public void markEmailAsNotSpam() {
        service.logIntoMailBoxWithUserCredentials(service.correctUserLogin, service.correctUserPassword);
        service.sendRandomEmailTo(service.mailToAddress, false);
        service.waitForElementPresent(service.DONE_LOCATOR).click();
        service.waitForElementPresent(service.INBOX_LINK_LOCATOR).click();
        service.markFirstEmailOnPage(service.INBOX_PAGE_CHECKBOX_LOCATOR);
        service.waitForElementPresent(service.SPAM_ICON_LOCATOR).click();
        service.goToPage(service.SPAM_LINK_LOCATOR);
        service.markFirstEmailOnPage(service.SPAM_PAGE_CHECKBOX_LOCATOR);
        service.waitForElementPresent(service.NOT_SPAM_ICON_LOCATOR).click();
        //Добавить проверку того что письмо отсутствует в папке спам
        service.goToPage(service.INBOX_LINK_LOCATOR);
        service.assertEmailPresentOnPage(service.MAIL_SUBJECT_LOCATOR_ON_SENT_PAGE, this.service.mailSubject);
    }
}
