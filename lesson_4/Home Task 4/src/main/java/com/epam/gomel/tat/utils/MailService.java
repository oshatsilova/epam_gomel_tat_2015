package com.epam.gomel.tat.utils;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.locks.Condition;

/**
 * Created by oshatsilova on 17.03.2015.
 */

// надо бы ctl+alt+L

public class MailService {  // думаю что после последних лекция понятно как использовать Service

    private WebDriver driver;

    MailService(WebDriver dr) {
        this.driver = dr;
    }

    // AUT data
    public static final String BASE_URL = "http://www.ya.ru";

    // UI locators
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By MAIL_ACCOUNT_LINK_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    public static final By ERROR_MAIL_LOGIN_MESSAGE_LOCATOR = By.className("error-msg");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INRUT_FILE_FIELD_LOCATOR = By.xpath("//input[@name='att']");
    //public static final By ATTACHMENT_LOCATOR = By.xpath("//img[@title='С вложением']");
    //public static final By DOWNLOAD_ATTACHMENT_LOCATOR = By.xpath("//button[@class=' nb-button _nb-small-button _init']");
    public static final By INBOX_PAGE_CHECKBOX_LOCATOR = By.xpath("//input[@class='b-messages__message__checkbox__input']");
    public static final By SPAM_PAGE_CHECKBOX_LOCATOR = By.xpath("//label[.='Спам']/ancestor::*[@class='block-messages']//input[@class='b-messages__message__checkbox__input']");
    public static final By DELETE_EMAIL_LOCATOR = By.xpath("//img[@class='b-ico b-ico_delete']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#sent']");
    public static final By TRESH_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#trash']");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#inbox']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link'][@href='#spam']");
    public static final By DONE_LOCATOR = By.xpath("//a[@class='b-done-redirect__link'][@href='#inbox']");
    public static final By SPAM_ICON_LOCATOR = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='tospam']");
    public static final By NOT_SPAM_ICON_LOCATOR = By.xpath("//div[@class='b-toolbar__block b-toolbar__block_chevron']//a[@data-action='notspam']");
    public static final By MAIL_SUBJECT_LOCATOR_ON_INBOX_PAGE = By.xpath("//span[@class='js-messages-title-dropdown-name'][.='shatsilova@yandex.ru']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']");
    public static final By MAIL_SUBJECT_LOCATOR_ON_SENT_PAGE = By.xpath("//label[.='Отправленные']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']");
    public static final By MAIL_SUBJECT_LOCATOR_ON_DELETED_PAGE = By.xpath("//label[.='Удалённые']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']");
    public static final By MAIL_SUBJECT_LOCATOR_ON_SPAM_PAGE = By.xpath("//label[.='Спам']/ancestor::*[@class='block-messages']//span[@class='b-messages__subject']");

    // UI data
    public String errorMailLoginMessage = "Неправильная пара логин-пароль! Авторизоваться не удалось.";
    public String partOfYandexMaiLAccounLink = "@yandex.ru";

    //Tolls data
    public static final int DRIVER_TOTAL_WAIT = 5000;

    // Test data
    public String correctUserLogin = "shatsilova"; // don't forget to change to test data
    public String correctUserPassword = "00223344"; // ACCOUNT
    public String mailToAddress = "shatsilova@yandex.ru"; // ENUM
    public String mailSubject = "test mail subject: " + Math.random() * 100000000;
    public String mailContent = "mail content: " + Math.random() * 100000000;
    public static String pathToFile = "C:\\TestAttachment.txt"; // и тут имя файла...


    public void verifyMailAccount() {
        WebElement mailAccountLink = driver.findElement(MAIL_ACCOUNT_LINK_LOCATOR);
        Assert.assertEquals(mailAccountLink.getText(), correctUserLogin + partOfYandexMaiLAccounLink, "Account is wrong");
    }

    public void logIntoMailBoxWithUserCredentials(String user_login, String user_password) {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(user_login);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(user_password);
        passInput.submit();
    }

    public void verifyMessage(By massegeLocator, String messageText) {
        WebElement errorMessage = driver.findElement(massegeLocator);
        Assert.assertEquals(errorMessage.getText(), messageText, "Massege is absent");
    }

    public void sendRandomEmailTo(String recipient, Boolean withAttachment) {
        WebElement composeButton = waitForElementPresent(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(recipient);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.clear();
        mailContentText.sendKeys(mailContent);
        if (withAttachment) {
            FileUtils.format(mailContent);
            WebElement attachFileButton = waitForElementInvisible(INRUT_FILE_FIELD_LOCATOR);
            attachFileButton.sendKeys(pathToFile);
        }
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    public WebElement waitForElementInvisible(By locator) {
        new WebDriverWait(driver, DRIVER_TOTAL_WAIT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementVisible(By locator) {
        new WebDriverWait(driver, DRIVER_TOTAL_WAIT).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementPresent(By locator) {
        new WebDriverWait(driver, DRIVER_TOTAL_WAIT).until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public void assertEmailPresentOnPage(By pageLocator, String messageTitle) {
        WebElement emailSubject = waitForElementPresent(pageLocator);
        Assert.assertEquals(emailSubject.getAttribute("title"), messageTitle, "Email is absent on the page");
    }

    public void markFirstEmailOnPage(By locator) {
        WebElement checkbox = waitForElementVisible(locator);
        checkbox.click();
    }

    public void goToPage(By pageLink) {
        new WebDriverWait(driver, DRIVER_TOTAL_WAIT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
        Actions action = new Actions(driver);
        WebElement elem = driver.findElement(pageLink);
        action.moveToElement(elem);
        action.perform();
        elem.click();
    }

}
