package com.epam.gomel.tat.utils; // это же не utils а тест, надо было его в пакет с тестами

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;


/**
 * Created by oshatsilova on 17.03.2015.
 */
public class MailTestsBase{

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;

    public WebDriver driver;
    public MailService service;
    public FileUtils file;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        //driver.manage().window().maximize(); - почему закомментировано?
        service = new MailService(driver);
        file = new FileUtils();
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }
}
